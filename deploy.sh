PORT="${SPORZ_PORT:-80}"
TAG="${SPORZ_TAG:-latest}"
CONTAINER_PORT="${CONTAINER_PORT:-8889}"

echo "Build ? y/n"
read ans
[[ $ans == "y" ]] && ./build.sh

container_name=sporz_"$TAG"
docker rm -f $container_name
docker run -d \
  -e PYTHONUNBUFFERED=1 \
  --name "$container_name" \
  -p "$CONTAINER_PORT":"$PORT" \
  -p 7070:5555 \
  -e SPORZ_PORT="$PORT" \
  -v $PWD/saves:/usr/src/app/saves \
  sporz:"$TAG"