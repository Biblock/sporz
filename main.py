import asyncio
import os

from src.server.server import Server

if __name__ == '__main__':
    event_loop = asyncio.new_event_loop()
    asyncio.set_event_loop(event_loop)
    port = os.environ.get("SPORZ_PORT", 8080)
    save_file_name = os.environ.get("SAVE_FILE_NAME", None)
    saves_folder = os.environ.get("SAVE_FOLDER", "saves")
    use_save = None
    if save_file_name is not None:
        save_file_path = os.path.join(saves_folder, save_file_name)
        if os.path.exists(save_file_path):
            use_save = save_file_name
        else:
            print(f"Save file {save_file_path} doesn't exist")
    game_server = Server(port, event_loop, saves_folder, use_save)
    game_server.start()
    event_loop.run_forever()
