# Sporz
## First round example
### Actions
```yaml
{"action": "LOGIN", "name": "Aurore"}
{"action": "LOGIN", "name": "Bryan"}
{"action": "LOGIN", "name": "Charlie"}
{"action": "LOGIN", "name": "Dylan"}
{"action": "LOGIN", "name": "Enzo"}
{"action": "LOGIN", "name": "Farid"}
{"action": "LOGIN", "name": "Gabrielle"}
{"action": "LOGIN", "name": "Hugo"}
{"action": "LOGIN", "name": "Ines"}
{"action": "LOGIN", "name": "Jade"}
{"action": "LOGIN", "name": "Kevin"}
{"action": "LOGIN", "name": "Lucie"}
{"action": "CREATE_GAME", "gameName": "Sporz", "sessionToken": "25642f7d-5ec7-42cc-996d-7b42895d0034"}
{"action": "JOIN_GAME", "gameId": "72dd9f86-3592-4655-9900-857258fc7659", "sessionToken": "25642f7d-5ec7-42cc-996d-7b42895d0034"}
{"action": "JOIN_GAME", "gameId": "72dd9f86-3592-4655-9900-857258fc7659", "sessionToken": "2777e663-6797-4a25-972a-6a74c1ead57c"}
{"action": "JOIN_GAME", "gameId": "72dd9f86-3592-4655-9900-857258fc7659", "sessionToken": "cc6f3633-bd58-4d3e-b9f6-337fa8074759"}
{"action": "JOIN_GAME", "gameId": "72dd9f86-3592-4655-9900-857258fc7659", "sessionToken": "12eb4a15-3c04-49ab-b258-c979e0963243"}
{"action": "JOIN_GAME", "gameId": "72dd9f86-3592-4655-9900-857258fc7659", "sessionToken": "0f16c903-ae82-4bf2-adfe-43deb479cda9"}
{"action": "JOIN_GAME", "gameId": "72dd9f86-3592-4655-9900-857258fc7659", "sessionToken": "7bae4089-33e4-4a4d-8624-2d7f953cd20c"}
{"action": "JOIN_GAME", "gameId": "72dd9f86-3592-4655-9900-857258fc7659", "sessionToken": "471debcb-a377-4daf-89a4-d6865d473fef"}
{"action": "JOIN_GAME", "gameId": "72dd9f86-3592-4655-9900-857258fc7659", "sessionToken": "edd3c64c-48ab-491c-b27c-2e1b6c1bffcc"}
{"action": "JOIN_GAME", "gameId": "72dd9f86-3592-4655-9900-857258fc7659", "sessionToken": "1fbd5711-9544-40ff-840e-7f57ecf60055"}
{"action": "JOIN_GAME", "gameId": "72dd9f86-3592-4655-9900-857258fc7659", "sessionToken": "a531ef0c-9043-4002-a49f-9ecb9f08f2b7"}
{"action": "JOIN_GAME", "gameId": "72dd9f86-3592-4655-9900-857258fc7659", "sessionToken": "cb0473e4-8e4d-44ae-b181-484165d2269d"}
{"action": "JOIN_GAME", "gameId": "72dd9f86-3592-4655-9900-857258fc7659", "sessionToken": "2e9eb127-9301-4f9b-887c-8a85b462287b"}
{"action": "START_GAME", "gameId": "72dd9f86-3592-4655-9900-857258fc7659", "sessionToken": "25642f7d-5ec7-42cc-996d-7b42895d0034"}
{"action": "VOTE_MUTATE", "targetId": "ca9d0f16-cd0b-4703-81ac-41bb5adc9e56", "sessionToken": "2777e663-6797-4a25-972a-6a74c1ead57c", "gameId": "72dd9f86-3592-4655-9900-857258fc7659"}
{"action": "VOTE_BRAINWASH", "targetId": "2270ac1e-4788-4178-bdd6-c5c70aea8476", "sessionToken": "2777e663-6797-4a25-972a-6a74c1ead57c", "gameId": "72dd9f86-3592-4655-9900-857258fc7659"}
{"action": "HEAL", "targetId": "1d2e7f70-41fe-495f-a079-421e41c3c1cb", "sessionToken": "0f16c903-ae82-4bf2-adfe-43deb479cda9", "gameId": "72dd9f86-3592-4655-9900-857258fc7659"}
{"action": "HEAL", "targetId": "76653be6-c074-46d0-991d-cf3485321763", "sessionToken": "cb0473e4-8e4d-44ae-b181-484165d2269d", "gameId": "72dd9f86-3592-4655-9900-857258fc7659"}
{"action": "PSYCHOLOGIST_ANALYSE", "targetId": "2270ac1e-4788-4178-bdd6-c5c70aea8476", "sessionToken": "a531ef0c-9043-4002-a49f-9ecb9f08f2b7", "gameId": "72dd9f86-3592-4655-9900-857258fc7659"}
{"action": "GENETICIST_ANALYSE", "targetId": "76653be6-c074-46d0-991d-cf3485321763", "sessionToken": "cc6f3633-bd58-4d3e-b9f6-337fa8074759", "gameId": "72dd9f86-3592-4655-9900-857258fc7659"}
{"action": "HACK", "hackedRole": "PSYCHOLOGIST", "sessionToken": "2e9eb127-9301-4f9b-887c-8a85b462287b", "gameId": "72dd9f86-3592-4655-9900-857258fc7659"}
{"action": "INDIC_INVESTIGATE", "targetId": "2270ac1e-4788-4178-bdd6-c5c70aea8476", "sessionToken": "25642f7d-5ec7-42cc-996d-7b42895d0034", "gameId": "72dd9f86-3592-4655-9900-857258fc7659"}
{"action": "INVESTIGATE", "targetId": "2270ac1e-4788-4178-bdd6-c5c70aea8476", "sessionToken": "1fbd5711-9544-40ff-840e-7f57ecf60055", "gameId": "72dd9f86-3592-4655-9900-857258fc7659"}
{"action": "VOTE_EXECUTION", "targetId": "76653be6-c074-46d0-991d-cf3485321763", "sessionToken": "cc6f3633-bd58-4d3e-b9f6-337fa8074759", "gameId": "72dd9f86-3592-4655-9900-857258fc7659"}
{"action": "VOTE_EXECUTION", "targetId": "ca9d0f16-cd0b-4703-81ac-41bb5adc9e56", "sessionToken": "2777e663-6797-4a25-972a-6a74c1ead57c", "gameId": "72dd9f86-3592-4655-9900-857258fc7659"}
{"action": "VOTE_EXECUTION", "targetId": null, "sessionToken": "7bae4089-33e4-4a4d-8624-2d7f953cd20c", "gameId": "72dd9f86-3592-4655-9900-857258fc7659"}
{"action": "VOTE_EXECUTION", "targetId": null, "sessionToken": "edd3c64c-48ab-491c-b27c-2e1b6c1bffcc", "gameId": "72dd9f86-3592-4655-9900-857258fc7659"}
{"action": "VOTE_EXECUTION", "targetId": null, "sessionToken": "2e9eb127-9301-4f9b-887c-8a85b462287b", "gameId": "72dd9f86-3592-4655-9900-857258fc7659"}
{"action": "VOTE_EXECUTION", "targetId": null, "sessionToken": "0f16c903-ae82-4bf2-adfe-43deb479cda9", "gameId": "72dd9f86-3592-4655-9900-857258fc7659"}
{"action": "VOTE_EXECUTION", "targetId": null, "sessionToken": "cb0473e4-8e4d-44ae-b181-484165d2269d", "gameId": "72dd9f86-3592-4655-9900-857258fc7659"}
{"action": "VOTE_EXECUTION", "targetId": null, "sessionToken": "cc6f3633-bd58-4d3e-b9f6-337fa8074759", "gameId": "72dd9f86-3592-4655-9900-857258fc7659"}
{"action": "VOTE_EXECUTION", "targetId": null, "sessionToken": "25642f7d-5ec7-42cc-996d-7b42895d0034", "gameId": "72dd9f86-3592-4655-9900-857258fc7659"}
{"action": "VOTE_EXECUTION", "targetId": null, "sessionToken": "a531ef0c-9043-4002-a49f-9ecb9f08f2b7", "gameId": "72dd9f86-3592-4655-9900-857258fc7659"}
{"action": "VOTE_EXECUTION", "targetId": null, "sessionToken": "1fbd5711-9544-40ff-840e-7f57ecf60055", "gameId": "72dd9f86-3592-4655-9900-857258fc7659"}
{"action": "VOTE_EXECUTION", "targetId": null, "sessionToken": "471debcb-a377-4daf-89a4-d6865d473fef", "gameId": "72dd9f86-3592-4655-9900-857258fc7659"}
{"action": "VOTE_EXECUTION", "targetId": null, "sessionToken": "12eb4a15-3c04-49ab-b258-c979e0963243", "gameId": "72dd9f86-3592-4655-9900-857258fc7659"}
```
### Events
```yaml
{"event": "LOGIN", "user": {"id": "ca9d0f16-cd0b-4703-81ac-41bb5adc9e56", "name": "Aurore"}}
{"event": "LOGIN", "user": {"id": "ca9d0f16-cd0b-4703-81ac-41bb5adc9e56", "name": "Aurore"}, "sessionToken": "25642f7d-5ec7-42cc-996d-7b42895d0034"}
{"event": "LOGIN", "user": {"id": "76653be6-c074-46d0-991d-cf3485321763", "name": "Bryan"}}
{"event": "LOGIN", "user": {"id": "76653be6-c074-46d0-991d-cf3485321763", "name": "Bryan"}, "sessionToken": "2777e663-6797-4a25-972a-6a74c1ead57c"}
{"event": "LOGIN", "user": {"id": "80a27691-d66a-47a8-9ca3-2e730463d558", "name": "Charlie"}}
{"event": "LOGIN", "user": {"id": "80a27691-d66a-47a8-9ca3-2e730463d558", "name": "Charlie"}, "sessionToken": "cc6f3633-bd58-4d3e-b9f6-337fa8074759"}
{"event": "LOGIN", "user": {"id": "8fae77a8-e4c1-4ece-bb89-a6ff53baf7c3", "name": "Dylan"}}
{"event": "LOGIN", "user": {"id": "8fae77a8-e4c1-4ece-bb89-a6ff53baf7c3", "name": "Dylan"}, "sessionToken": "12eb4a15-3c04-49ab-b258-c979e0963243"}
{"event": "LOGIN", "user": {"id": "3231d04e-5b93-49ed-85ad-91610e262223", "name": "Enzo"}}
{"event": "LOGIN", "user": {"id": "3231d04e-5b93-49ed-85ad-91610e262223", "name": "Enzo"}, "sessionToken": "0f16c903-ae82-4bf2-adfe-43deb479cda9"}
{"event": "LOGIN", "user": {"id": "2270ac1e-4788-4178-bdd6-c5c70aea8476", "name": "Farid"}}
{"event": "LOGIN", "user": {"id": "2270ac1e-4788-4178-bdd6-c5c70aea8476", "name": "Farid"}, "sessionToken": "7bae4089-33e4-4a4d-8624-2d7f953cd20c"}
{"event": "LOGIN", "user": {"id": "40f855cc-0bec-47c3-b270-915e958e5114", "name": "Gabrielle"}}
{"event": "LOGIN", "user": {"id": "40f855cc-0bec-47c3-b270-915e958e5114", "name": "Gabrielle"}, "sessionToken": "471debcb-a377-4daf-89a4-d6865d473fef"}
{"event": "LOGIN", "user": {"id": "70776968-a5d1-4aaa-a333-10dcae2536a4", "name": "Hugo"}}
{"event": "LOGIN", "user": {"id": "70776968-a5d1-4aaa-a333-10dcae2536a4", "name": "Hugo"}, "sessionToken": "edd3c64c-48ab-491c-b27c-2e1b6c1bffcc"}
{"event": "LOGIN", "user": {"id": "546028d9-ad37-4d8c-855f-fdbaa32b0ab3", "name": "Ines"}}
{"event": "LOGIN", "user": {"id": "546028d9-ad37-4d8c-855f-fdbaa32b0ab3", "name": "Ines"}, "sessionToken": "1fbd5711-9544-40ff-840e-7f57ecf60055"}
{"event": "LOGIN", "user": {"id": "1d2e7f70-41fe-495f-a079-421e41c3c1cb", "name": "Jade"}}
{"event": "LOGIN", "user": {"id": "1d2e7f70-41fe-495f-a079-421e41c3c1cb", "name": "Jade"}, "sessionToken": "a531ef0c-9043-4002-a49f-9ecb9f08f2b7"}
{"event": "LOGIN", "user": {"id": "cceca4d9-5bb6-45cb-9c92-9f4a66a74617", "name": "Kevin"}}
{"event": "LOGIN", "user": {"id": "cceca4d9-5bb6-45cb-9c92-9f4a66a74617", "name": "Kevin"}, "sessionToken": "cb0473e4-8e4d-44ae-b181-484165d2269d"}
{"event": "LOGIN", "user": {"id": "dfae300e-e298-41a9-b368-4a178d3a09c1", "name": "Lucie"}}
{"event": "LOGIN", "user": {"id": "dfae300e-e298-41a9-b368-4a178d3a09c1", "name": "Lucie"}, "sessionToken": "2e9eb127-9301-4f9b-887c-8a85b462287b"}
{"event": "GAME_CREATED", "game": {"gameName": "Sporz", "gameId": "72dd9f86-3592-4655-9900-857258fc7659", "admin": {"id": "ca9d0f16-cd0b-4703-81ac-41bb5adc9e56", "name": "Aurore"}, "minNumberOfPlayer": 11}}
{"event": "JOINED_GAME", "game": {"gameName": "Sporz", "gameId": "72dd9f86-3592-4655-9900-857258fc7659", "admin": {"id": "ca9d0f16-cd0b-4703-81ac-41bb5adc9e56", "name": "Aurore"}, "minNumberOfPlayer": 11}, "user": {"id": "ca9d0f16-cd0b-4703-81ac-41bb5adc9e56", "name": "Aurore"}}
{"event": "JOINED_GAME", "game": {"gameName": "Sporz", "gameId": "72dd9f86-3592-4655-9900-857258fc7659", "admin": {"id": "ca9d0f16-cd0b-4703-81ac-41bb5adc9e56", "name": "Aurore"}, "minNumberOfPlayer": 11}, "user": {"id": "76653be6-c074-46d0-991d-cf3485321763", "name": "Bryan"}}
{"event": "JOINED_GAME", "game": {"gameName": "Sporz", "gameId": "72dd9f86-3592-4655-9900-857258fc7659", "admin": {"id": "ca9d0f16-cd0b-4703-81ac-41bb5adc9e56", "name": "Aurore"}, "minNumberOfPlayer": 11}, "user": {"id": "80a27691-d66a-47a8-9ca3-2e730463d558", "name": "Charlie"}}
{"event": "JOINED_GAME", "game": {"gameName": "Sporz", "gameId": "72dd9f86-3592-4655-9900-857258fc7659", "admin": {"id": "ca9d0f16-cd0b-4703-81ac-41bb5adc9e56", "name": "Aurore"}, "minNumberOfPlayer": 11}, "user": {"id": "8fae77a8-e4c1-4ece-bb89-a6ff53baf7c3", "name": "Dylan"}}
{"event": "JOINED_GAME", "game": {"gameName": "Sporz", "gameId": "72dd9f86-3592-4655-9900-857258fc7659", "admin": {"id": "ca9d0f16-cd0b-4703-81ac-41bb5adc9e56", "name": "Aurore"}, "minNumberOfPlayer": 11}, "user": {"id": "3231d04e-5b93-49ed-85ad-91610e262223", "name": "Enzo"}}
{"event": "JOINED_GAME", "game": {"gameName": "Sporz", "gameId": "72dd9f86-3592-4655-9900-857258fc7659", "admin": {"id": "ca9d0f16-cd0b-4703-81ac-41bb5adc9e56", "name": "Aurore"}, "minNumberOfPlayer": 11}, "user": {"id": "2270ac1e-4788-4178-bdd6-c5c70aea8476", "name": "Farid"}}
{"event": "JOINED_GAME", "game": {"gameName": "Sporz", "gameId": "72dd9f86-3592-4655-9900-857258fc7659", "admin": {"id": "ca9d0f16-cd0b-4703-81ac-41bb5adc9e56", "name": "Aurore"}, "minNumberOfPlayer": 11}, "user": {"id": "40f855cc-0bec-47c3-b270-915e958e5114", "name": "Gabrielle"}}
{"event": "JOINED_GAME", "game": {"gameName": "Sporz", "gameId": "72dd9f86-3592-4655-9900-857258fc7659", "admin": {"id": "ca9d0f16-cd0b-4703-81ac-41bb5adc9e56", "name": "Aurore"}, "minNumberOfPlayer": 11}, "user": {"id": "70776968-a5d1-4aaa-a333-10dcae2536a4", "name": "Hugo"}}
{"event": "JOINED_GAME", "game": {"gameName": "Sporz", "gameId": "72dd9f86-3592-4655-9900-857258fc7659", "admin": {"id": "ca9d0f16-cd0b-4703-81ac-41bb5adc9e56", "name": "Aurore"}, "minNumberOfPlayer": 11}, "user": {"id": "546028d9-ad37-4d8c-855f-fdbaa32b0ab3", "name": "Ines"}}
{"event": "JOINED_GAME", "game": {"gameName": "Sporz", "gameId": "72dd9f86-3592-4655-9900-857258fc7659", "admin": {"id": "ca9d0f16-cd0b-4703-81ac-41bb5adc9e56", "name": "Aurore"}, "minNumberOfPlayer": 11}, "user": {"id": "1d2e7f70-41fe-495f-a079-421e41c3c1cb", "name": "Jade"}}
{"event": "JOINED_GAME", "game": {"gameName": "Sporz", "gameId": "72dd9f86-3592-4655-9900-857258fc7659", "admin": {"id": "ca9d0f16-cd0b-4703-81ac-41bb5adc9e56", "name": "Aurore"}, "minNumberOfPlayer": 11}, "user": {"id": "cceca4d9-5bb6-45cb-9c92-9f4a66a74617", "name": "Kevin"}}
{"event": "JOINED_GAME", "game": {"gameName": "Sporz", "gameId": "72dd9f86-3592-4655-9900-857258fc7659", "admin": {"id": "ca9d0f16-cd0b-4703-81ac-41bb5adc9e56", "name": "Aurore"}, "minNumberOfPlayer": 11}, "user": {"id": "dfae300e-e298-41a9-b368-4a178d3a09c1", "name": "Lucie"}}
{"event": "GAME_STARTED", "game": {"gameName": "Sporz", "gameId": "72dd9f86-3592-4655-9900-857258fc7659", "admin": {"id": "ca9d0f16-cd0b-4703-81ac-41bb5adc9e56", "name": "Aurore"}, "minNumberOfPlayer": 11}, "users": [{"id": "ca9d0f16-cd0b-4703-81ac-41bb5adc9e56", "name": "Aurore"}, {"id": "76653be6-c074-46d0-991d-cf3485321763", "name": "Bryan"}, {"id": "80a27691-d66a-47a8-9ca3-2e730463d558", "name": "Charlie"}, {"id": "8fae77a8-e4c1-4ece-bb89-a6ff53baf7c3", "name": "Dylan"}, {"id": "3231d04e-5b93-49ed-85ad-91610e262223", "name": "Enzo"}, {"id": "2270ac1e-4788-4178-bdd6-c5c70aea8476", "name": "Farid"}, {"id": "40f855cc-0bec-47c3-b270-915e958e5114", "name": "Gabrielle"}, {"id": "70776968-a5d1-4aaa-a333-10dcae2536a4", "name": "Hugo"}, {"id": "546028d9-ad37-4d8c-855f-fdbaa32b0ab3", "name": "Ines"}, {"id": "1d2e7f70-41fe-495f-a079-421e41c3c1cb", "name": "Jade"}, {"id": "cceca4d9-5bb6-45cb-9c92-9f4a66a74617", "name": "Kevin"}, {"id": "dfae300e-e298-41a9-b368-4a178d3a09c1", "name": "Lucie"}]}
{"event": "NEW_NIGHT"}
{"phase": "FIRST_MUTANT_PHASE", "event": "NEW_PHASE"}
{"voter": {"id": "76653be6-c074-46d0-991d-cf3485321763", "name": "Bryan"}, "target": {"id": "ca9d0f16-cd0b-4703-81ac-41bb5adc9e56", "name": "Aurore"}, "event": "VOTE_MUTATE"}
{"target": {"id": "ca9d0f16-cd0b-4703-81ac-41bb5adc9e56", "name": "Aurore"}, "event": "RESISTED_MUTATION"}
{"phase": "SECOND_MUTANT_PHASE", "event": "NEW_PHASE"}
{"voter": {"id": "76653be6-c074-46d0-991d-cf3485321763", "name": "Bryan"}, "target": {"id": "2270ac1e-4788-4178-bdd6-c5c70aea8476", "name": "Farid"}, "event": "VOTE_BRAIN_WASH"}
{"target": {"id": "2270ac1e-4788-4178-bdd6-c5c70aea8476", "name": "Farid"}, "event": "MUTANT_BRAIN_WASHED"}
{"phase": "DOCTOR_PHASE", "event": "NEW_PHASE"}
{"target": {"id": "1d2e7f70-41fe-495f-a079-421e41c3c1cb", "name": "Jade"}, "event": "RESISTED_HEALING"}
{"target": {"id": "76653be6-c074-46d0-991d-cf3485321763", "name": "Bryan"}, "event": "RESISTED_HEALING"}
{"phase": "PSYCHOLOGIST_PHASE", "event": "NEW_PHASE"}
{"target": {"id": "2270ac1e-4788-4178-bdd6-c5c70aea8476", "name": "Farid"}, "mutant": true, "event": "PSYCHOLOGIST_ANALYSIS"}
{"phase": "COMPUTER_SCIENTIST_PHASE", "event": "NEW_PHASE"}
{"mutant_number": 1, "event": "COMPUTER_SCIENTIST_REPORT"}
{"phase": "GENETICIST_PHASE", "event": "NEW_PHASE"}
{"target": {"id": "76653be6-c074-46d0-991d-cf3485321763", "name": "Bryan"}, "genome": "Genome.HOST", "event": "GENETICIST_ANALYSIS"}
{"phase": "HACKER_PHASE", "event": "NEW_PHASE"}
{"target": {"id": "2270ac1e-4788-4178-bdd6-c5c70aea8476", "name": "Farid"}, "mutant": true, "event": "PSYCHOLOGIST_ANALYSIS"}
{"phase": "INDIC_PHASE", "event": "NEW_PHASE"}
{"target": {"id": "2270ac1e-4788-4178-bdd6-c5c70aea8476", "name": "Farid"}, "traitor": true, "event": "INDIC_REPORT"}
{"phase": "INVESTIGATOR_PHASE", "event": "NEW_PHASE"}
{"target": {"id": "2270ac1e-4788-4178-bdd6-c5c70aea8476", "name": "Farid"}, "woke_up": false, "mutated": false, "paralysed": false, "brain_washed": true, "healed": false, "psychologist_analysed": true, "geneticist_analysed": false, "inspected": false, "spied_on": false, "event": "INVESTIGATOR_REPORT"}
{"phase": "NEW_DAY", "event": "NEW_PHASE"}
{"target": null, "event": "VOTE_RESULT"}
{"event": "NEW_NIGHT"}
{"phase": "FIRST_MUTANT_PHASE", "event": "NEW_PHASE"}
```