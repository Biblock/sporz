
var app = new Vue({
  el: '#app',
  data: {
    socket: null,
    newGameName: "",
    user: null,
    username: "",
    users: [],
    games: [],
    selected: null,
    reportContent: ""
  },
  computed: {
    currentGame: function() {
      return this.games.find(x => x.started && x.users.map(u => u.id).includes(this.user.id))
    },
    thisAstronaut: function() {
      return this.currentGame.astronauts[this.user.id]
    },
    currentGameReady: function() {
      return this.currentGame && this.currentGame.phase && this.currentGame.astronauts
    },
    turnIsOver: function() {
      return this.thisAstronaut.turn_is_over
    },
    currentPhaseDisplay: function() {
      phase = this.currentGame.phase
      phase_repr = phase.role
      if (phase.role == "Mutant") {
        mutant_phase = phase.name.replaceAll('_', ' ').toLowerCase()
        mutant_phase = mutant_phase[0].toUpperCase() + mutant_phase.slice(1);
        phase_repr += ` (${mutant_phase})`
      }
      return phase_repr
    },
    currentPromptOptions: function() {
      cpo = {}
      currentGame = this.currentGame
      selectUsersPhases = ["FIRST_MUTANT_PHASE", "SECOND_MUTANT_PHASE", "DOCTOR_PHASE",
        "PSYCHOLOGIST_PHASE", "GENETICIST_PHASE", "INDIC_PHASE", "SPY_PHASE", "NEW_DAY", "INVESTIGATOR_PHASE"]
      if (selectUsersPhases.includes(this.currentGame.phase.name)) {
        cpo["options"] = this.currentGame.users.map(function(usr) {
          return {"prompt": usr.name, "value": usr, "disabled": !currentGame.astronauts[usr.id].alive}
        })
      } else if (this.currentGame.phase.name == "HACKER_PHASE") {
        cpo["options"] = [
          {"prompt": "Psychologist", value: "PSYCHOLOGIST", disabled: false},
          {"prompt": "Geneticist", value: "GENETICIST", disabled: false},
          {"prompt": "Computer Scientist", value: "COMPUTERSCIENTIST", disabled: false}
        ]
      }

      if (this.currentGame.phase.name === "FIRST_MUTANT_PHASE") {
        cpo["actions"] = [{"id": "VOTE_MUTATE", "prompt": "Mutate"}, {"id": "VOTE_MUTANT_KILL", "prompt": "Kill"}]
      } else if (this.currentGame.phase.name === "SECOND_MUTANT_PHASE") {
        cpo["actions"] = [{"id": "VOTE_PARALYSE", "prompt": "Paralyse"}, {"id": "VOTE_BRAINWASH", "prompt": "Brainwash"}]
      } else if (this.currentGame.phase.name === "DOCTOR_PHASE") {
        cpo["actions"] = [{"id": "HEAL", "prompt": "Heal"}, {"id": "VOTE_DOCTOR_KILL", "prompt": "Kill"}]
      } else if (this.currentGame.phase.name === "PSYCHOLOGIST_PHASE") {
        cpo["actions"] = [{"id": "PSYCHOLOGIST_ANALYSE", "prompt": "Analyse"}]
      } else if (this.currentGame.phase.name === "COMPUTER_SCIENTIST_PHASE") {
        cpo["actions"] = [{"id": "COUNT_MUTANTS", "prompt": "Get mutant count"}]
      } else if (this.currentGame.phase.name === "GENETICIST_PHASE") {
        cpo["actions"] = [{"id": "GENETICIST_ANALYSE", "prompt": "Analyse"}]
      } else if (this.currentGame.phase.name === "HACKER_PHASE") {
        cpo["actions"] = [{"id": "HACK", "prompt": "Hack"}]
      } else if (this.currentGame.phase.name === "INDIC_PHASE") {
        cpo["actions"] = [{"id": "INDIC_INVESTIGATE", "prompt": "Investigate"}]
      } else if (this.currentGame.phase.name === "SPY_PHASE") {
        cpo["actions"] = [{"id": "SPY", "prompt": "Spy"}]
      } else if (this.currentGame.phase.name === "INVESTIGATOR_PHASE") {
        cpo["actions"] = [{"id": "INVESTIGATE", "prompt": "Investigate"}]
      } else if (this.currentGame.phase.name === "NEW_DAY") {
        cpo["actions"] = [{"id": "VOTE_EXECUTION", "prompt": "Vote"}, {"id": "VOTE_NONE", "prompt": "Blank vote"}]
      }
      return cpo
    },
  },
  methods: {
    send: function(data) {
      console.log(JSON.parse(data))
      this.socket.send(data)
    },
    login: function() {
      this.send(`{"action": "LOGIN", "name": "${this.username}"}`)
      this.username = ""
    },
    closeSocket: function() {
      this.socket.close()
      this.socket = null
    },
    newGame: function() {
      this.send(`{"action": "CREATE_GAME", "gameName": "${this.newGameName}", "sessionToken": "${this.user.sessionToken}"}, "autoNextPhase": true`)
      this.newGameName = ""
    },
    startGame: function(game) {
      this.send(`{"action": "START_GAME", "gameId": "${game.id}", "sessionToken": "${this.user.sessionToken}"}`)
    },
    deleteGame: function(game) {
      this.send(`{"action": "DELETE_GAME", "gameId": "${game.id}", "sessionToken": "${this.user.sessionToken}"}`)
    },
    updateGame: function(game) {
        Vue.set(this.games, this.games.indexOf(this.games.find(x => x.id == game.id)), game)
    },
    joinGame: function(game) {
      this.send(`{"action": "JOIN_GAME", "gameId": "${game.id}", "sessionToken": "${this.user.sessionToken}"}`)
    },
    select: function(user) {
      this.selected = user
      if (this.currentGame.phase.name === "DOCTOR_PHASE") {
        this.send(`{"action": "SELECT_HEAL", "targetId": "${this.selected.id}", "gameId": "${this.currentGame.id}", "sessionToken": "${this.user.sessionToken}"}`)

      }
    },
    refreshGame: function() {
      if (this.currentGame) {
        this.send(`{"action": "GET_GAME", "sessionToken": "${this.user.sessionToken}", "gameId": "${this.currentGame.id}"}`)
      }
    },
    validateAction: function(action) {
      if (action.id == "COUNT_MUTANTS") {
        this.send(`{"action": "${action.id}", "gameId": "${this.currentGame.id}", "sessionToken": "${this.user.sessionToken}"}`)
      } else if (action.id == "HACK") {
        this.send(`{"action": "${action.id}", "hackedRole": "${this.selected}", "gameId": "${this.currentGame.id}", "sessionToken": "${this.user.sessionToken}"}`)
      } else {
        if (action.id == "VOTE_NONE") {
          this.send(`{"action": "VOTE_EXECUTION", "targetId": null, "gameId": "${this.currentGame.id}", "sessionToken": "${this.user.sessionToken}"}`)
        } else if (this.selected) {
          this.send(`{"action": "${action.id}", "targetId": "${this.selected.id}", "gameId": "${this.currentGame.id}", "sessionToken": "${this.user.sessionToken}"}`)
        }
      }
      this.selected = null
    },
    endTurn: function() {
      this.send(`{"action": "NEXT_PHASE", "gameId": "${this.currentGame.id}", "sessionToken": "${this.user.sessionToken}"}`)
    },
    showReport: function(content) {
      reportModal = new bootstrap.Modal(document.getElementById('reportModal'))
      this.reportContent = content
      reportModal.show()
    },
    socketOnCreated: function() {
      queryString = window.location.search
      urlParams = new URLSearchParams(queryString)
      this.username = urlParams.get('username')
      if (this.username) {
        this.login()
      }
    },
    socketOnMessage: function(event) {
      event = JSON.parse(event.data)
      console.log(event)
      if (event.event === "LOGIN") {
        user = event.user
        if ("sessionToken" in event) {
          user.sessionToken = event.sessionToken
          this.user = user
          this.send(`{"action": "GET_LOBBY", "sessionToken": "${this.user.sessionToken}"}`)
        }
        if (!this.users.map(x => x.id).includes(user.id)) {
          this.users.push(user)
        }
      } else if (event.event === "GAME") {
        Vue.set(this.currentGame, 'phase', event.phase)
        Vue.set(this.currentGame, 'astronauts', event.astronauts)
      } else if (event.event === "LOBBY") {
        this.users = event.users
        this.games = event.games
        if (this.currentGame) {
          this.refreshGame()
        }
      } else if (event.event === "LOGOUT") {
        i = this.users.indexOf(this.users.find(x => x.id === event.user.id))
        if (i != -1) {
          this.users.splice(i, 1)
        }
      } else if (event.event === "GAME_CREATED") {
        if (!this.games.map(x => x.id).includes(event.game.id)) {
          this.games.push(event.game)
        }
      } else if (event.event === "NEW_PHASE") {
        Vue.set(this.currentGame, 'phase', event.phase)
        this.refreshGame()
      } else if (event.event === "GAME_STARTED") {
        this.updateGame(event.game)
        this.refreshGame()
      } else if (event.event === "JOINED_GAME") {
        this.updateGame(event.game)
      } else if (event.event === "PSYCHOLOGIST_ANALYSIS") {
        this.showReport(`${event.target.name} is ${event.mutant ? '' : 'not '}a mutant`)
      } else if (event.event === "COMPUTER_SCIENTIST_REPORT") {
        this.showReport(`There are ${event.mutant_number} mutant(s)`)
      } else if (event.event === "GENETICIST_ANALYSIS") {
        this.showReport(`${event.target.name} genome is ${event.genome}`)
      } else if (event.error === "HackedRoleNotPresent" || event.error === "HackedRoleParalysed") {
        this.showReport("Nothing to hack, make the conclusion you want")
      } else if (event.event === "GAME_DELETED") {
        this.send(`{"action": "GET_LOBBY", "sessionToken": "${this.user.sessionToken}"}`)
      } else if (event.event === "TURN_OVER") {
        this.thisAstronaut.turn_is_over = true
      } else if (event.event === "PHASE_OVER") {
//        this.endTurn()
      } else if (event.event === "INDIC_REPORT") {
        this.showReport(`${event.target.name} is ${event.traitor ? '' : 'not '}a traitor`)
      } else if (event.event === "SPY_REPORT") {
        report_events =["woke_up", "mutated", "paralysed", "brain_washed", "healed", "psychologist_analysed",
          "geneticist_analysed", "inspected", "investigated"]
        report = report_events.map(x => `${x}: ${event[x]}`).join("<br />")
        this.showReport(report)
      } else if (event.event === "INVESTIGATOR_REPORT") {
        report_events =["woke_up", "mutated", "paralysed", "brain_washed", "healed", "psychologist_analysed",
          "geneticist_analysed", "inspected", "spied_on"]
        report = report_events.map(x => `${x}: ${event[x]}`).join("<br />")
        this.showReport(report)
      } else if (event.event === "VOTE_RESULT") {
        if (event.target) {
          this.showReport(`You chose to kill ${event.target.name}<br />Roles: ${event.roles.join(', ')}<br />Genome: ${event.genome}`)
        } else {
          this.showReport("You chose not to kill anyone today")
        }
      }
    },
  },
  created: function() {
    this.socket = new WebSocket("ws://localhost:8080")
//    this.socket = new WebSocket("wss://biblock.fr/ws_sporz")
    this.socket.onmessage = this.socketOnMessage
    this.socket.onopen = this.socketOnCreated
  }
})
