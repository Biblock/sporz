names = ["Aurore", "Bryan", "Charlie", "Dylan", "Enzo", "Farid", "Gabrielle", "Hugo", "Ines", "Jade", "Kevin", "Lucie",
         "Marie", "Noemie", "Oscar", "Pavel", "Quentin", "Renaud", "Sophie", "Tony", "Ulysse", "Valentin", "William",
         "Xavier", "Yannis", "Zoe"]


var app = new Vue({
  el: '#app',
  data: {
    socket: null,
    game: null,
    phase: null,
    logged: [],
    inGame: [],
    operator: null
  },
  methods: {
    send: function(data) {
      console.log(JSON.parse(data))
      this.socket.send(data)
    },
    login: function() {
      name = names[this.logged.length]
      this.send(`{"action": "LOGIN", "name": "${name}"}`)
    },
    mega_login: function() {
      for (x of Array(15).keys()) {
        this.send(`{"action": "LOGIN", "name": "${names[x]}"}`)
      }

    },
    join_all: function() {
      for (usr of this.logged) {
        this.send(`{"action": "JOIN_GAME", "gameId": "${this.game.id}", "sessionToken": "${usr.sessionToken}"}`)
      }
    },
    createGame: function(user) {
      this.send(`{"action": "CREATE_GAME", "gameName": "The Game", "sessionToken": "${user.sessionToken}", "autoNextPhase": true}`)
    },
    joinGame: function(user) {
      this.send(`{"action": "JOIN_GAME", "gameId": "${this.game.id}", "sessionToken": "${user.sessionToken}"}`)
    },
    joinGameOperator: function(user) {
      this.send(`{"action": "JOIN_GAME", "asOperator": true, "gameId": "${this.game.id}", "sessionToken": "${user.sessionToken}"}`)
    },
    startGame: function(user) {
      this.send(`{"action": "START_GAME", "gameId": "${this.game.id}", "sessionToken": "${user.sessionToken}"}`)
    },
    getGameInfo: function(user) {
      this.send(`{"action": "GET_GAME", "gameId": "${this.game.id}", "sessionToken": "${user.sessionToken}"}`)
    },
    getLobbyInfo: function(user) {
      this.send(`{"action": "GET_LOBBY", "sessionToken": "${user.sessionToken}"}`)
    },
    save_server: function() {
      this.send(`{"action": "SAVE", "sessionToken": "${user.sessionToken}"}`)
    },
    socketOnMessage: function(event) {
      event = JSON.parse(event.data)
      console.log(event)
      if (event.event === "LOGIN" && "sessionToken" in event) {
        user = event.user
        user.sessionToken = event.sessionToken
        this.logged.push(user)
      }
      if (event.event === "GAME_CREATED") {
        this.game = event.game
      }
      if (event.event === "GAME") {
        const key = Object.keys(event.astronauts).find(key => event.astronauts[key].roles)
        var self = event.astronauts[key]
        Vue.set(this.logged.find(element => element.id == event.recipientId), 'roles', self.roles)
        this.phase = event.phase
      }
      if (event.event === "LOBBY") {
        var theGame = event.games.find((g => g.name === "The Game"))
        if (theGame) {
          this.game = theGame
          this.inGame = this.game.users.map(u => u.id)
          for (const user of this.logged) {
            console.log(user);
            this.getGameInfo(user)
          }
        }
      }
      if (event.event === "NEW_PHASE") {
        this.phase = event.phase
        this.getLobbyInfo(this.logged[0])
      }
      if (event.event === "JOINED_GAME") {
        if (event.asOperator) {
          this.operator = event.user.id
        } else {
          if (!this.inGame.includes(event.user.id)) {
            this.inGame.push(event.user.id)
          }
        }
      }
      if (event.event === "GAME_STARTED") {
        this.game = event.game
      }
      if (event.event === "ROLE_ATTRIBUTION" && event.recipientId != this.operator) {
        Vue.set(this.logged.find(element => element.id == event.recipientId), 'roles', [event.role])
      }
    },
  },
  created: function() {
    this.socket = new WebSocket("ws://localhost:8080")
//    this.socket = new WebSocket("wss://biblock.fr/ws_sporz")
    this.socket.onmessage = this.socketOnMessage
  }
})
