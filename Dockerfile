FROM python:3.8

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY src ./src
COPY main.py ./
COPY run.sh ./
COPY index.html ./
COPY script.js ./
COPY tool.html ./
COPY script_tool.js ./
COPY operator.html ./
COPY script_operator.js ./

ENV TZ=Europe/Paris

CMD [ "./run.sh" ]
