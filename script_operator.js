
var app = new Vue({
  el: '#app',
  data: {
    socket: null,
    games: [],
    user: null,
    currentGameId: null,
  },
  computed: {
    currentGame: function() {
      return this.games.find(x => x.id == this.currentGameId)
    }
  },
  methods: {
    send: function(data) {
      console.log(JSON.parse(data))
      this.socket.send(data)
    },
    login: function() {
      this.send(`{"action": "LOGIN", "name": "Operator"}`)
    },
    socketOnCreated: function() {
      this.login()
    },
    joinGame: function(game) {
      this.send(`{"action": "JOIN_GAME", "asOperator": true, "gameId": "${game.id}", "sessionToken": "${this.user.sessionToken}"}`)
    },
    followGame: function(game) {

      var msg = new SpeechSynthesisUtterance();
      this.currentGameId = game.id
      this.refreshGame()
    },
    updateGame: function(game) {
      Vue.set(this.games, this.games.indexOf(this.games.find(x => x.id == game.id)), game)
    },
    refreshGame: function() {
      if (this.currentGame) {
        this.send(`{"action": "GET_GAME", "sessionToken": "${this.user.sessionToken}", "gameId": "${this.currentGame.id}"}`)
      }
    },
    socketOnmessage: function(event) {
      event = JSON.parse(event.data)
      console.log(event)
      if (event.event === "LOGIN") {
        user = event.user
        if ("sessionToken" in event) {
          user.sessionToken = event.sessionToken
          this.user = user
          this.send(`{"action": "GET_LOBBY", "sessionToken": "${this.user.sessionToken}"}`)
        }
      } else if (event.event === "GAME") {
        Vue.set(this.currentGame, 'phase', event.phase)
        Vue.set(this.currentGame, 'astronauts', event.astronauts)
      } else if (event.event === "LOBBY") {
        this.users = event.users
        this.games = event.games
        if (this.currentGame) {
          this.refreshGame()
        }
      } else if (event.event === "GAME_CREATED") {
        if (!this.games.map(x => x.id).includes(event.game.id)) {
          this.games.push(event.game)
        }
      } else if (event.event === "JOINED_GAME") {
        this.updateGame(event.game)
      } else if (event.event === "GAME_STARTED") {
        this.updateGame(event.game)
        this.refreshGame()
      } else if (event.event === "NEW_PHASE") {
        msg = new SpeechSynthesisUtterance()
        msg.text = `New phase: ${event.phase.role}`
        msg.lang = "en"
        window.speechSynthesis.speak(msg)

      } else if (event.event === "VOTE_RESULT") {

      }
    },
  },
  created: function() {
    this.socket = new WebSocket("ws://localhost:8080")
//    this.socket = new WebSocket("wss://biblock.fr/ws_sporz")
    this.socket.onmessage = this.socketOnmessage
    this.socket.onopen = this.socketOnCreated
  }
})
