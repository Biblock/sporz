from copy import copy
from dataclasses import dataclass
from enum import Enum, auto

from src.engine.exceptions import DoesntHaveTheRole


class Genome(Enum):
    NEUTRAL = auto()
    HOST = auto()
    RESISTANT = auto()

    def __str__(self):
        return self.name


@dataclass
class Report:
    woke_up: bool = False
    mutated: bool = False
    paralysed: bool = False
    brain_washed: bool = False
    healed: bool = False
    psychologist_analysed: bool = False
    geneticist_analysed: bool = False
    inspected: bool = False
    investigated: bool = False
    spied_on: bool = False
    # below some info that doesn't appear in spy/investigator reports
    killed: bool = False
    resisted_mutation: bool = False
    resisted_healing: bool = False


class Astronaut:
    def __init__(self, game, genome, role=None):
        self.game = game
        self.genome = genome
        self.alive = True
        self.paralysed = False
        self.brain_washed = False
        self.roles = set() if role is None else {role(self)}
        self.report = None
        self.old_report = None
        self.room = None
        self.night_reports = []
        self.mutant_last_night = False

    def __repr__(self):
        return f"{'Alive' if self.alive else 'Dead'}, {self.genome}, {self.roles}"

    def reset(self):
        self.paralysed = False
        self.brain_washed = False
        self.night_reports = []
        for role in self.roles:
            role.__init__(self)

    def as_role(self, role):
        try:
            return next(r for r in self.roles if type(r) == role)
        except StopIteration:
            raise DoesntHaveTheRole

    def has_role(self, role_type):
        return any(type(r) == role_type for r in self.roles)

    @property
    def active(self):
        return self.alive and not self.paralysed

    def to_json(self, public_info_only=False):
        data = {
            "alive": self.alive
        }
        if not public_info_only:
            data["roles"] = ["Astronaut"] if not self.roles else [str(role) for role in self.roles]
            data["inactive_roles"] = [str(role) for role in self.roles if role.incapacitated]
            data["paralysed"] = self.paralysed
            data["brain_washed"] = self.brain_washed
            data["resisted_mutation"] = self.report.resisted_mutation
            data["resisted_healing"] = self.report.resisted_healing
            data["mutated"] = self.report.mutated
            data["healed"] = self.report.healed
            data["killed"] = self.report.killed
            data["turn_is_over"] = self in self.game.phase.action_done
        return data
