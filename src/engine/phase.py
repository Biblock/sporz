from abc import ABC
from collections import Counter
from datetime import datetime

import inflection

from src.engine.engine_events import VoteResult, TurnOver
from src.engine.roles.computer_scientist import ComputerScientist
from src.engine.roles.doctor import Doctor
from src.engine.roles.geneticist import Geneticist
from src.engine.roles.hacker import Hacker
from src.engine.roles.indic import Indic
from src.engine.roles.investigator import Investigator
from src.engine.roles.mutant import Mutant
from src.engine.roles.psychologist import Psychologist
from src.engine.roles.spy import Spy


class Phase:
    ACTIVE_ROLE = None

    def __init__(self, game):
        self.game = game
        self.next_phase_ready = []
        self.action_done = []
        self.ignored_for_readiness = []
        self.start_date = datetime.now()
        self.duration = 600

    @property
    def is_over(self):
        raise NotImplementedError

    @property
    def active_role(self):
        return self.ACTIVE_ROLE

    def when_over(self):
        raise NotImplementedError

    def __repr__(self):
        return inflection.underscore(self.__class__.__name__).upper()

    def to_json(self):
        return {
            "name": str(self),
            "role": self.ACTIVE_ROLE.__name__ if self.ACTIVE_ROLE is not None else "Any",
            "startDate": int(self.start_date.timestamp()),
            "duration": self.duration
        }

    def all_next_phase_ready(self):
        return all(ast in self.next_phase_ready or ast in self.ignored_for_readiness
                   for ast in self.game.get_all(self.ACTIVE_ROLE, as_astronaut=True, active=True))

    def mark_ready(self, astronaut):
        self.next_phase_ready.append(astronaut)

    def mark_action_done(self, astronaut):
        self.action_done.append(astronaut)
        self.game.add_event(TurnOver([astronaut], self.to_json()))

    def mark_ignore_for_readiness(self, astronaut):
        self.ignored_for_readiness.append(astronaut)


class RolePhase(Phase, ABC):
    def when_over(self):
        self.game.next_phase()


class FirstMutantPhase(RolePhase):
    ACTIVE_ROLE = Mutant

    def __init__(self, game):
        super().__init__(game)
        self.has_mutated = False
        self.has_killed = False
        self.has_paralysed = False
        self.has_brain_washed = False

    @property
    def is_over(self):
        return self.has_mutated or self.has_killed


class SecondMutantPhase(RolePhase):
    ACTIVE_ROLE = Mutant

    def __init__(self, game):
        super().__init__(game)
        self.has_paralysed = False
        self.has_brain_washed = False

    @property
    def is_over(self):
        return self.has_paralysed or self.has_brain_washed


class DoctorPhase(RolePhase):
    ACTIVE_ROLE = Doctor

    def __init__(self, game):
        super().__init__(game)
        self.has_healed = False
        self.has_killed = False

    @property
    def is_over(self):
        return self.has_healed or self.has_killed


class BasicRolePhase(RolePhase):
    def __init__(self, game):
        super().__init__(game)

    @property
    def is_over(self):
        return all(a in self.action_done for a in self.game.get_all(self.ACTIVE_ROLE, as_astronaut=True, active=True))


class PsychologistPhase(BasicRolePhase):
    ACTIVE_ROLE = Psychologist


class GeneticistPhase(BasicRolePhase):
    ACTIVE_ROLE = Geneticist


class HackerPhase(BasicRolePhase):
    ACTIVE_ROLE = Hacker


class IndicPhase(BasicRolePhase):
    ACTIVE_ROLE = Indic


class InvestigatorPhase(BasicRolePhase):
    ACTIVE_ROLE = Investigator


class SpyPhase(BasicRolePhase):
    ACTIVE_ROLE = Spy


class ComputerScientistPhase(BasicRolePhase):
    ACTIVE_ROLE = ComputerScientist


class VotePhase(Phase):
    def __init__(self, game, alive_astronauts):
        super().__init__(game)
        self.votes = {ast: False for ast in alive_astronauts}

    @property
    def is_over(self):
        return not any(v is False for v in self.votes.values())

    def vote(self, voter, target):
        self.votes[voter] = target

    def vote_for_non_voters(self):
        for ast, vote in self.votes.items():
            if vote is False:
                self.votes[ast] = None

    def when_over(self):
        choice = Counter(self.votes.values()).most_common(1)[0][0]
        if choice is not None:
            choice.alive = False
            choice_info = choice.to_json(public_info_only=False)
            self.game.add_event(VoteResult(self.game.astronauts, choice, choice_info["roles"], str(choice.genome)))
        else:
            self.game.add_event(VoteResult(self.game.astronauts, choice, None, None))

        self.game.new_night()

    def all_next_phase_ready(self):
        return self.is_over

    def __repr__(self):
        return "NEW_DAY"
