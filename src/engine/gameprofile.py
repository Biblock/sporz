from random import shuffle

from src.engine.astronaut import Genome, Astronaut
from src.engine.exceptions import NotEnoughAstronautsError
from src.engine.enums import ROLES
from src.engine.roles.doctor import Doctor
from src.engine.roles.mutant import Mutant


class GameProfile:
    def __init__(self, disabled_roles=None, resistant_number=1, non_mutant_host_number=1, initial_roles_number=None,
                 delayed_psychologist=True, auto_next_phase=False):
        if initial_roles_number is None:
            initial_roles_number = {}
        self.disabled_roles = [] if disabled_roles is None else [
            r for r in disabled_roles if not r.MANDATORY
        ]
        self.resistant_number = resistant_number
        self.non_mutant_host_number = non_mutant_host_number
        self.initial_roles_number = {
            role:
                max(initial_roles_number[role], 0 if not role.MANDATORY else 1) if role in initial_roles_number else
                role.MINIMUM_INITIAL_ASTRONAUT_NUMBER
            for role in ROLES
        }
        self.delayed_psychologist = delayed_psychologist
        self.auto_next_phase = auto_next_phase

    @property
    def non_neutral_number(self):
        # +1 because the mutant won't have one of those genomes
        return self.non_mutant_host_number + self.resistant_number + 1

    @property
    def min_initial_astronaut_number(self):
        return max(self.non_neutral_number,
                   sum(self.initial_roles_number[r] if r not in self.disabled_roles else 0 for r in ROLES))

    def create_astronauts(self, game, astronaut_number):
        if astronaut_number < self.min_initial_astronaut_number:
            raise NotEnoughAstronautsError
        genome_pool = [*(self.resistant_number * [Genome.RESISTANT]),
                       *(self.non_mutant_host_number * [Genome.HOST])]
        shuffle(genome_pool)
        role_pool = []
        for role in ROLES:
            if role not in self.disabled_roles:
                role_pool.extend((self.initial_roles_number[role] * [role]))
        astronauts = []
        for _ in range(astronaut_number):
            try:
                role = role_pool.pop(0)
            except IndexError:
                role = None
            if role is Mutant:
                genome = Genome.HOST
            elif role is Doctor:
                genome = Genome.NEUTRAL
            else:
                genome = genome_pool.pop(0) if len(genome_pool) > 0 else Genome.NEUTRAL

            astronauts.append(Astronaut(game, genome, role))
        shuffle(astronauts)
        return astronauts
