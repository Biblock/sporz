class EngineError(Exception):
    pass


class DoesntHaveTheRole(EngineError):
    pass


class CanNotHaveRole(EngineError):
    def __init__(self, role):
        self.role = role


class NotEnoughAstronautsError(EngineError):
    pass


class WrongPhaseError(EngineError):
    pass


class PhaseOverError(EngineError):
    pass


class UnauthorizedRoleError(EngineError):
    pass


class AstronautIsDeadError(EngineError):
    pass


class IncapacitatedError(EngineError):
    pass


class AstronautIsParalysedError(IncapacitatedError):
    pass


class HasHealedAlreadyError(EngineError):
    pass


class MutantNewlyMutatedError(IncapacitatedError):
    pass


class MutantDoctorError(IncapacitatedError):
    pass


class AstronautsCanNotBeFromDifferentGamesError(EngineError):
    pass


class HackedRoleNotPresent(EngineError):
    pass


class HackedRoleParalysed(EngineError):
    pass


class NoRoomLeftError(EngineError):
    def __init__(self, room_name):
        self.room_name = room_name


class NoSuchRoomError(EngineError):
    def __init__(self, room_name):
        self.room_name = room_name
