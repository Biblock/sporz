from src.engine.phase import FirstMutantPhase, SecondMutantPhase, DoctorPhase, BasicRolePhase, PsychologistPhase, \
    GeneticistPhase, IndicPhase, InvestigatorPhase, SpyPhase, ComputerScientistPhase, HackerPhase
from src.engine.roles.doctor import Doctor
from src.engine.roles.mutant import Mutant
from src.engine.roles.hacker import Hacker
from src.engine.roles.geneticist import Geneticist
from src.engine.roles.indic import Indic
from src.engine.roles.investigator import Investigator
from src.engine.roles.spy import Spy
from src.engine.roles.traitor import Traitor
from src.engine.roles.computer_scientist import ComputerScientist
from src.engine.roles.psychologist import Psychologist

ROLES = (Mutant, Doctor, Psychologist, ComputerScientist, Geneticist, Hacker, Indic, Investigator, Spy, Traitor)
PHASE_ORDER = [
    FirstMutantPhase,
    SecondMutantPhase,
    DoctorPhase,
    PsychologistPhase,
    ComputerScientistPhase,
    GeneticistPhase,
    HackerPhase,
    IndicPhase,
    InvestigatorPhase,
    SpyPhase,
]
