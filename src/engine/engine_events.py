from dataclasses import dataclass
from typing import List

import inflection

from src.engine.astronaut import Astronaut


@dataclass
class EngineEvent:
    recipients: List

    def __str__(self):
        return self.__class__.__name__

    def get_name(self):
        return inflection.underscore(self.__class__.__name__).upper()


@dataclass
class EnterRoom(EngineEvent):
    astronaut: Astronaut
    room_name: str


@dataclass
class RoomPermission(EngineEvent):
    room_full: bool
    room_name: str


@dataclass
class RoleAttribution(EngineEvent):
    role: str


@dataclass
class VoteEvent(EngineEvent):
    voter: Astronaut
    target: Astronaut


@dataclass
class Target(EngineEvent):
    target: Astronaut


@dataclass
class VoteMutate(VoteEvent):
    pass


@dataclass
class VoteMutantKill(VoteEvent):
    pass


@dataclass
class VoteDoctorKill(VoteEvent):
    pass


@dataclass
class VoteParalyse(VoteEvent):
    pass


@dataclass
class VoteBrainWash(VoteEvent):
    pass


@dataclass
class Mutation(Target):
    pass


@dataclass
class Healed(Target):
    pass


@dataclass
class SelectHeal(VoteEvent):
    pass


@dataclass
class SelectMutate(VoteEvent):
    pass


@dataclass
class SelectDoctorKill(VoteEvent):
    pass


@dataclass
class SelectMutantKill(VoteEvent):
    pass


@dataclass
class SelectBrainwash(VoteEvent):
    pass


@dataclass
class SelectParalyse(VoteEvent):
    pass


@dataclass
class MutantKill(Target):
    pass


@dataclass
class DoctorKill(Target):
    pass


@dataclass
class MutantParalysed(Target):
    pass


@dataclass
class MutantBrainWashed(Target):
    pass


@dataclass
class ResistedMutation(Target):
    pass


@dataclass
class ResistedHealing(Target):
    pass


@dataclass
class VoteResult(Target):
    roles: list
    genome: str


@dataclass
class PsychologistAnalysis(Target):
    mutant: bool


@dataclass
class GeneticistAnalysis(Target):
    genome: str


@dataclass
class IndicReport(Target):
    traitor: bool


@dataclass
class HackingFailed(EngineEvent):
    pass


@dataclass
class NewPhase(EngineEvent):
    phase: dict


@dataclass
class PhaseOver(EngineEvent):
    phase: dict


@dataclass
class NewRound(EngineEvent):
    def get_name(self):
        return "NEW_NIGHT"


@dataclass
class ComputerScientistReport(EngineEvent):
    mutant_number: int


@dataclass
class MatesStatuses(EngineEvent):
    role: str
    disabled: list
    not_disabled: list


@dataclass
class Report(Target):
    woke_up: bool
    mutated: bool
    paralysed: bool
    brain_washed: bool
    healed: bool
    psychologist_analysed: bool
    geneticist_analysed: bool
    inspected: bool


@dataclass
class InvestigatorReport(Report):
    spied_on: bool


@dataclass
class SpyReport(Report):
    investigated: bool


@dataclass
class TurnOver(EngineEvent):
    phase: dict
