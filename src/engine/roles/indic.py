from src.engine.engine_events import IndicReport
from src.engine.roles.role import Role
from src.engine.roles.traitor import Traitor


class Indic(Role):
    def inspect(self, target):
        game = self.astronaut.game
        report = IndicReport([self.astronaut], target, target.has_role(Traitor))
        game.add_event(report)
        self.astronaut.report.inspected = True
        self.astronaut.night_reports.append(report)
        game.phase.mark_action_done(self.astronaut)
