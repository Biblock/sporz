from src.engine.engine_events import HackingFailed
from src.engine.exceptions import HackedRoleNotPresent, HackedRoleParalysed
from src.engine.roles.role import Role


class Hacker(Role):
    def hack(self, role):
        game = self.astronaut.game
        hacked_set = game.get_all(role, alive=True)
        failed = True

        for hacked in hacked_set:
            hacked_report = hacked.report
            if hacked_report is None:
                continue
            hacked_report.recipients = [self.astronaut]
            game.add_event(hacked_report)
            self.astronaut.night_reports.append(hacked_report)
            failed = False

        if failed:
            hacking_failed = HackingFailed([self.astronaut])
            game.add_event(hacking_failed)
            self.astronaut.night_reports.append(hacking_failed)
        game.phase.mark_action_done(self.astronaut)
