from src.engine.engine_events import GeneticistAnalysis
from src.engine.roles.role import Hackable


class Geneticist(Hackable):
    def analyse(self, target):
        game = self.astronaut.game
        self.report = GeneticistAnalysis([self.astronaut], target, str(target.genome))
        target.report.geneticist_analysed = True
        game.add_event(self.report)
        self.astronaut.night_reports.append(self.report)
        game.phase.mark_action_done(self.astronaut)
