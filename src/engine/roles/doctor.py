from src.engine.astronaut import Genome
from src.engine.engine_events import Healed, ResistedHealing, VoteDoctorKill, DoctorKill, SelectHeal, SelectDoctorKill
from src.engine.exceptions import MutantDoctorError, HasHealedAlreadyError
from src.engine.roles.mutant import Mutant
from src.engine.roles.role import Killer


class Doctor(Killer):
    MANDATORY = True
    MINIMUM_INITIAL_ASTRONAUT_NUMBER = 2

    def __init__(self, astronaut):
        super().__init__(astronaut, VoteDoctorKill, DoctorKill)
        self.kill_vote = None
        self.has_healed = False

    def heal(self, target):
        game = self.astronaut.game
        skip_target = False
        if self.has_healed:
            raise HasHealedAlreadyError
        if target.genome is not Genome.HOST:
            game.add_event(Healed([target], target))
            if target.has_role(Mutant):
                target.roles.remove(next(r for r in target.roles if type(r) is Mutant))
                target.report.healed = True
                if target.has_role(Doctor):
                    skip_target = True
                    game.phase.mark_ignore_for_readiness(target)  # can't I just set it ready ?

        else:
            target.report.resisted_healing = True
            game.add_event(ResistedHealing([target], target))

        self.has_healed = True
        game.phase.mark_action_done(self.astronaut)
        if all(d.has_healed for d in game.get_all(Doctor, active=True) if not (skip_target and d.astronaut == target)):
            game.phase.has_healed = True

    def select_heal(self, target):
        game = self.astronaut.game
        doctors = game.get_all(Doctor, as_astronaut=True, active=True)
        game.add_event(SelectHeal(doctors, self.astronaut, target))

    def select_kill(self, target):
        game = self.astronaut.game
        doctors = game.get_all(Doctor, as_astronaut=True, active=True)
        game.add_event(SelectDoctorKill(doctors, self.astronaut, target))

    def other_active_doctor(self):
        try:
            return next(d for d in self.astronaut.game.get_all(Doctor, as_astronaut=True, active=True) if d is not self)
        except StopIteration:
            return None

    def check_incapacitated(self):
        if self.astronaut.has_role(Mutant):
            raise MutantDoctorError
