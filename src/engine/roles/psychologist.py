from src.engine.engine_events import PsychologistAnalysis
from src.engine.roles.mutant import Mutant
from src.engine.roles.role import Hackable


class Psychologist(Hackable):
    def analyse(self, target):
        game = self.astronaut.game
        if game.game_profile.delayed_psychologist:
            real_mutation_status = target.mutant_last_night
            was_brainwashed_during_relevant_turn = target.old_report.brain_washed if target.old_report is not None else False
        else:
            real_mutation_status = target.has_role(Mutant)
            was_brainwashed_during_relevant_turn = target.brain_washed
        sent_mutation_status = not real_mutation_status if was_brainwashed_during_relevant_turn else real_mutation_status
        self.report = PsychologistAnalysis([self.astronaut], target, sent_mutation_status)
        target.report.psychologist_analysed = True
        game.add_event(self.report)
        self.astronaut.night_reports.append(self.report)
        game.phase.mark_action_done(self.astronaut)
