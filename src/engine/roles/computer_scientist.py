from src.engine.engine_events import ComputerScientistReport
from src.engine.roles.mutant import Mutant
from src.engine.roles.role import Hackable


class ComputerScientist(Hackable):
    def count_mutants(self):
        game = self.astronaut.game
        self.report = ComputerScientistReport(
            game.get_all(ComputerScientist, as_astronaut=True, active=True),
            len(game.get_all(Mutant, alive=True)))
        game.add_event(self.report)
        self.astronaut.night_reports.append(self.report)
        game.phase.mark_action_done(self.astronaut)
