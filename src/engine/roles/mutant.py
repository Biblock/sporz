from src.engine.astronaut import Genome
from src.engine.engine_events import VoteMutate, Mutation, ResistedMutation, VoteMutantKill, MutantKill, VoteParalyse, \
    MutantParalysed, VoteBrainWash, MutantBrainWashed, SelectMutantKill, SelectBrainwash, SelectParalyse, SelectMutate
from src.engine.exceptions import MutantNewlyMutatedError
from src.engine.roles.role import Killer


class Mutant(Killer):
    MANDATORY = True

    def __init__(self, astronaut, newly_mutated=False):
        super().__init__(astronaut, VoteMutantKill, MutantKill)
        self.mutation_vote = None
        self.paralysis_vote = None
        self.brain_wash_vote = None
        self.newly_mutated = newly_mutated

    def vote_mutate(self, target):
        game = self.astronaut.game
        self.mutation_vote = target
        game.add_event(VoteMutate(game.get_all(Mutant, as_astronaut=True), self.astronaut, target))
        active_mutants = game.get_all(Mutant, active=True)
        if all(p.mutation_vote == target for p in active_mutants):
            game.phase.has_mutated = True
            for m in active_mutants:
                game.phase.mark_action_done(m.astronaut)
            if target.genome is not Genome.RESISTANT:
                target.roles.add(Mutant(target, newly_mutated=True))
                target.report.mutated = True
                game.add_event(Mutation([target], target))
            else:
                target.report.resisted_mutation = True
                game.add_event(ResistedMutation([target], target))

    def vote_paralyse(self, target):
        game = self.astronaut.game
        self.paralysis_vote = target
        game.add_event(VoteParalyse(game.get_all(Mutant, as_astronaut=True), self.astronaut, target))
        active_mutants = game.get_all(Mutant, active=True)
        if all(p.paralysis_vote == target for p in active_mutants):
            for m in active_mutants:
                game.phase.mark_action_done(m.astronaut)
            target.paralysed = True
            target.report.paralysed = True
            game.phase.has_paralysed = True
            game.add_event(MutantParalysed([target], target))

    def vote_brainwash(self, target):
        game = self.astronaut.game
        self.brain_wash_vote = target
        game.add_event(VoteBrainWash(game.get_all(Mutant, as_astronaut=True), self.astronaut, target))
        active_mutants = game.get_all(Mutant, active=True)
        if all(p.brain_wash_vote == target for p in active_mutants):
            for m in active_mutants:
                game.phase.mark_action_done(m.astronaut)
            target.brain_washed = True
            target.report.brain_washed = True
            game.phase.has_brain_washed = True
            game.add_event(MutantBrainWashed([target], target))

    def select_mutate(self, target):
        game = self.astronaut.game
        mutants = game.get_all(Mutant, as_astronaut=True, active=True)
        game.add_event(SelectMutate(mutants, self.astronaut, target))

    def select_kill(self, target):
        game = self.astronaut.game
        mutants = game.get_all(Mutant, as_astronaut=True, active=True)
        game.add_event(SelectMutantKill(mutants, self.astronaut, target))

    def select_brainwash(self, target):
        game = self.astronaut.game
        mutants = game.get_all(Mutant, as_astronaut=True, active=True)
        game.add_event(SelectBrainwash(mutants, self.astronaut, target))

    def select_paralyse(self, target):
        game = self.astronaut.game
        mutants = game.get_all(Mutant, as_astronaut=True, active=True)
        game.add_event(SelectParalyse(mutants, self.astronaut, target))

    def check_incapacitated(self):
        if self.newly_mutated:
            raise MutantNewlyMutatedError
