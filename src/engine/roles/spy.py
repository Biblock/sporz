from src.engine.engine_events import SpyReport
from src.engine.roles.role import Role


class Spy(Role):
    def spy(self, target):
        game = self.astronaut.game
        report = target.report
        spy_report = SpyReport(
            [self.astronaut], target,
            woke_up=report.woke_up,
            mutated=report.mutated,
            paralysed=report.paralysed,
            brain_washed=report.brain_washed,
            healed=report.healed,
            psychologist_analysed=report.psychologist_analysed,
            geneticist_analysed=report.geneticist_analysed,
            inspected=report.inspected,
            investigated=report.investigated
        )
        game.add_event(spy_report)
        game.phase.mark_action_done(self.astronaut)
        self.astronaut.night_reports.append(spy_report)
        self.astronaut.report.spied_on = True
