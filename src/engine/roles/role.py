from src.engine.exceptions import IncapacitatedError


class Role:
    MINIMUM_INITIAL_ASTRONAUT_NUMBER = 1
    MANDATORY = False

    def __init__(self, astronaut):
        self.astronaut = astronaut

    def __repr__(self):
        return self.__class__.__name__

    def check_incapacitated(self):
        pass

    @property
    def incapacitated(self):
        try:
            self.check_incapacitated()
        except IncapacitatedError:
            return True
        return False

    @property
    def disabled(self):
        return self.astronaut.paralysed or self.incapacitated


class Killer(Role):
    def __init__(self, astronaut, vote_event, kill_event):
        super().__init__(astronaut)
        self.kill_vote = None
        self.kill_event = kill_event
        self.vote_event = vote_event

    def vote_kill(self, target):
        game = self.astronaut.game
        self.kill_vote = target
        game.add_event(self.vote_event(game.get_all(type(self), as_astronaut=True), self.astronaut, target))
        active_killers = game.get_all(type(self), active=True)
        if all(p.kill_vote == target for p in active_killers):
            target.alive = False
            target.report.killed = True
            game.phase.has_killed = True
            for k in active_killers:
                game.phase.mark_action_done(k.astronaut)
            game.add_event(self.kill_event(game.get_all(type(self), as_astronaut=True), target))


class Hackable(Role):
    def __init__(self, astronaut):
        super().__init__(astronaut)
        self.report = None
