from src.engine.engine_events import InvestigatorReport
from src.engine.roles.role import Role


class Investigator(Role):
    def investigate(self, target):
        game = self.astronaut.game
        report = target.old_report
        investigator_report = InvestigatorReport(
            [self.astronaut], target,
            woke_up=report.woke_up,
            mutated=report.mutated,
            paralysed=report.paralysed,
            brain_washed=report.brain_washed,
            healed=report.healed,
            psychologist_analysed=report.psychologist_analysed,
            geneticist_analysed=report.geneticist_analysed,
            inspected=report.inspected,
            spied_on=report.spied_on
        )
        game.add_event(investigator_report)
        game.phase.mark_action_done(self.astronaut)
        self.astronaut.night_reports.append(investigator_report)
        self.astronaut.report.investigated = True
