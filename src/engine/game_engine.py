from src.engine.astronaut import Report
from src.engine.engine_events import NewPhase, NewRound, RoleAttribution, PhaseOver, EnterRoom, RoomPermission, \
    MatesStatuses
from src.engine.enums import PHASE_ORDER
from src.engine.exceptions import DoesntHaveTheRole, NoRoomLeftError, NoSuchRoomError
from src.engine.gameprofile import GameProfile
from src.engine.interface import GameInterface
from src.engine.phase import VotePhase
from src.engine.roles.investigator import Investigator
from src.engine.roles.mutant import Mutant
from src.engine.room import Room


class GameEngine:
    def __init__(self, observer, game_profile=None, astronaut_number=None):
        self.observer = observer
        self.interface = GameInterface(self)
        self.phase_iterator, self.phase = None, None
        game_profile = GameProfile() if game_profile is None else game_profile
        self.game_profile = game_profile
        astronaut_number = game_profile.min_initial_astronaut_number + 2 if astronaut_number is None \
            else astronaut_number
        self.astronauts = game_profile.create_astronauts(self, astronaut_number)
        self.round_number = 0
        self.rooms = []
        self.init_rooms()

    def init_rooms(self):
        default_room_size = 4
        self.rooms.append(Room("COCKPIT", 5))
        self.rooms.append(Room("REFECTORY", 3))
        self.rooms.append(Room("OBSERVATORY", 3))
        self.rooms.append(Room("MACHINES", 4))
        self.rooms.append(Room("LABORATORY", 2))

    def get_all(self, role_type, as_astronaut=False, alive=None, active=None):
        found_astronauts = set()
        for ast in self.astronauts:
            try:
                if role_type is None or ast.has_role(role_type) and (alive is None or ast.alive == alive) and (
                        active is None or (ast.active == active and ast.as_role(role_type).incapacitated != active)):
                    found_astronauts.add(ast if as_astronaut else ast.as_role(role_type))
            except DoesntHaveTheRole:
                pass
        return found_astronauts

    def add_event(self, event):
        self.observer.add_event(event)

    def proceed(self, auto_next_phase=False):
        auto_next_phase = auto_next_phase or self.game_profile.auto_next_phase
        if self.phase.is_over:
            self.add_event(PhaseOver(list(self.get_all(self.phase.ACTIVE_ROLE, as_astronaut=True, active=True)),
                                     self.phase.to_json()))
        if auto_next_phase:
            self.resolve_phase_if_ready(auto_next_phase)

    def resolve_phase_if_ready(self, auto_next_phase):
        if self.phase.is_over and (self.phase.all_next_phase_ready() or auto_next_phase):
            self.phase.when_over()

    def new_night(self, first_round=False):
        for ast in self.astronauts:
            ast.roles = {type(role)(ast) for role in ast.roles}
            ast.old_report = ast.report
            ast.mutant_last_night = ast.has_role(Mutant)
            ast.report = Report()
        self.add_event(NewRound(self.astronauts))
        self.reset_roles_phase(first_round)
        self.reset_astronauts()
        self.round_number += 1

    def reset_roles_phase(self, first_round):
        still_alive_roles = set()
        for ast in self.astronauts:
            if ast.alive:
                for role in ast.roles:
                    still_alive_roles.add(type(role))
        self.phase_iterator = iter([
            phase for phase in PHASE_ORDER
            if phase.ACTIVE_ROLE in still_alive_roles and not (first_round and phase.ACTIVE_ROLE is Investigator)])
        self.next_phase()

    def next_phase(self):
        try:
            self.phase = next(self.phase_iterator)(self)
        except StopIteration:
            self.phase = VotePhase(self, [ast for ast in self.astronauts if ast.alive])
        self.add_event(NewPhase(self.astronauts, self.phase.to_json()))
        active_astronauts = self.get_all(self.phase.ACTIVE_ROLE, as_astronaut=True, active=True)
        for ast in active_astronauts:
            self.mates_statuses(ast)
        if not active_astronauts:
            self.next_phase()
        for ast in active_astronauts:
            ast.report.woke_up = True

    def start(self):
        for ast in self.astronauts:
            self.add_event(RoleAttribution([ast], str(list(ast.roles)[0]) if ast.roles else "Astronaut"))
        self.new_night(first_round=True)

    def reset_astronauts(self):
        for ast in self.astronauts:
            ast.reset()

    def enter_room(self, astronaut, room):
        self.check_room_free(astronaut, room, raise_error=True)
        astronaut.room = room
        self.add_event(EnterRoom([astronaut], astronaut, room.name))

    def check_room_free(self, astronaut, room, raise_error=False):
        room_occupants = [a for a in self.astronauts if a.room == room]
        if len(room_occupants) >= room.size:
            if raise_error:
                raise NoRoomLeftError(room.name)
            room_full = True
        else:
            room_full = False
        self.add_event(RoomPermission([astronaut], room_full, room.name))

    def get_room(self, room_name):
        try:
            return next(room for room in self.rooms if room.name == room_name)
        except StopIteration:
            raise NoSuchRoomError(room_name)

    def mates_statuses(self, astronaut):
        for role in astronaut.roles:
            role_type = type(role)
            all_playing_roles = self.get_all(role_type)
            disabled = [r.astronaut for r in all_playing_roles if r.disabled]
            not_disabled = [r.astronaut for r in all_playing_roles if not r.disabled]
            self.add_event(MatesStatuses([astronaut], str(role), disabled, not_disabled))
