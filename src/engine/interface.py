from src.engine.exceptions import WrongPhaseError, UnauthorizedRoleError, AstronautIsDeadError, \
    AstronautsCanNotBeFromDifferentGamesError, AstronautIsParalysedError, CanNotHaveRole, PhaseOverError
from src.engine.phase import SecondMutantPhase, FirstMutantPhase, DoctorPhase, VotePhase, PsychologistPhase, \
    GeneticistPhase, IndicPhase, InvestigatorPhase, SpyPhase, HackerPhase, ComputerScientistPhase
from src.engine.roles.computer_scientist import ComputerScientist
from src.engine.roles.doctor import Doctor
from src.engine.roles.geneticist import Geneticist
from src.engine.roles.hacker import Hacker
from src.engine.roles.indic import Indic
from src.engine.roles.investigator import Investigator
from src.engine.roles.mutant import Mutant
from src.engine.roles.psychologist import Psychologist
from src.engine.roles.spy import Spy
from src.server.exceptions import CannotHackThisRoleError


class GameInterface:
    def __init__(self, game):
        self.game = game

    @staticmethod
    def check_astronaut_role_is_not(target, role):
        if target.has_role(role):
            raise CanNotHaveRole(role)

    def check_same_game(self, *astronauts):
        for ast in astronauts:
            if ast.game != self.game:
                raise AstronautsCanNotBeFromDifferentGamesError
            if not ast.alive:
                raise AstronautIsDeadError

    @staticmethod
    def validate_astronaut_role(astronaut, allowed_role):
        if not astronaut.has_role(allowed_role):
            raise UnauthorizedRoleError

    def validate(self, astronaut, allowed_phase=None, can_be_incapacitated=False):
        phase = self.game.phase
        allowed_role = phase.active_role

        if allowed_phase is not None and type(phase) != allowed_phase:
            raise WrongPhaseError
        if allowed_role is not None:
            self.validate_astronaut_role(astronaut, allowed_role)
        if phase.is_over:
            raise PhaseOverError
        if not can_be_incapacitated:
            if astronaut.paralysed:
                raise AstronautIsParalysedError
            astronaut.as_role(allowed_role).check_incapacitated()

    def vote_mutate(self, mutant, target):
        self.validate(mutant, FirstMutantPhase)
        self.check_same_game(mutant, target)
        self.check_astronaut_role_is_not(target, Mutant)

        mutant.as_role(Mutant).vote_mutate(target)

        self.game.proceed()

    def vote_mutant_kill(self, mutant, target):
        self.validate(mutant, FirstMutantPhase)
        self.check_same_game(mutant, target)
        self.check_astronaut_role_is_not(target, Mutant)

        mutant.as_role(Mutant).vote_kill(target)

        self.game.proceed()

    def vote_paralyse(self, mutant, target):
        self.validate(mutant, SecondMutantPhase)
        self.check_same_game(mutant, target)

        mutant.as_role(Mutant).vote_paralyse(target)

        self.game.proceed()

    def vote_brainwash(self, mutant, target):
        self.validate(mutant, SecondMutantPhase)
        self.check_same_game(mutant, target)

        mutant.as_role(Mutant).vote_brainwash(target)

        self.game.proceed()

    def vote_doctor_kill(self, doctor, target):
        self.validate(doctor, DoctorPhase)
        self.check_same_game(doctor, target)
        self.check_astronaut_role_is_not(target, Doctor)

        doctor.as_role(Doctor).vote_kill(target)

        self.game.proceed()

    def select_heal(self, doctor, target):
        self.validate_astronaut_role(doctor, allowed_role=Doctor)
        self.check_same_game(doctor, target)

        doctor.as_role(Doctor).select_heal(target)

    def select_doctor_kill(self, doctor, target):
        self.validate_astronaut_role(doctor, allowed_role=Doctor)
        self.check_same_game(doctor, target)

        doctor.as_role(Doctor).select_kill(target)

    def select_mutate(self, mutant, target):
        self.validate_astronaut_role(mutant, allowed_role=Mutant)
        self.check_same_game(mutant, target)

        mutant.as_role(Mutant).select_mutate(target)

    def select_mutant_kill(self, mutant, target):
        self.validate_astronaut_role(mutant, allowed_role=Mutant)
        self.check_same_game(mutant, target)

        mutant.as_role(Mutant).select_kill(target)

    def select_brainwash(self, mutant, target):
        self.validate_astronaut_role(mutant, allowed_role=Mutant)
        self.check_same_game(mutant, target)

        mutant.as_role(Mutant).select_brainwash(target)

    def select_paralyse(self, mutant, target):
        self.validate_astronaut_role(mutant, allowed_role=Mutant)
        self.check_same_game(mutant, target)

        mutant.as_role(Mutant).select_paralyse(target)

    def heal(self, doctor, target):
        self.validate(doctor, DoctorPhase)
        self.check_same_game(doctor, target)

        doctor.as_role(Doctor).heal(target)

        self.game.proceed()

    def vote_execution(self, voter, target):
        self.validate(voter, VotePhase, can_be_incapacitated=True)
        if target is not None:
            self.check_same_game(voter, target)

        self.game.phase.vote(voter, target)

        self.game.proceed(auto_next_phase=True)

    def end_execution_votes(self):
        self.validate(None, VotePhase, can_be_incapacitated=True)
        self.game.phase.vote_for_non_voters()
        self.game.proceed(auto_next_phase=True)

    def psychologist_analysis(self, psychologist, target):
        self.validate(psychologist, PsychologistPhase)
        self.check_same_game(psychologist, target)

        psychologist.as_role(Psychologist).analyse(target)

        self.game.proceed()

    def geneticist_analysis(self, geneticist, target):
        self.validate(geneticist, GeneticistPhase)
        self.check_same_game(geneticist, target)

        geneticist.as_role(Geneticist).analyse(target)

        self.game.proceed()

    def indic_investigation(self, indic, target):
        self.validate(indic, IndicPhase)
        self.check_same_game(indic, target)

        indic.as_role(Indic).inspect(target)

        self.game.proceed()

    def hack(self, hacker, hacked_role):
        self.validate(hacker, HackerPhase)
        if hacked_role == "GENETICIST":
            role = Geneticist
        elif hacked_role == "PSYCHOLOGIST":
            role = Psychologist
        elif hacked_role == "COMPUTERSCIENTIST":
            role = ComputerScientist
        else:
            raise CannotHackThisRoleError

        hacker.as_role(Hacker).hack(role)

        self.game.proceed()

    def investigate(self, investigator, target):
        self.validate(investigator, InvestigatorPhase)
        self.check_same_game(investigator, target)

        investigator.as_role(Investigator).investigate(target)

        self.game.proceed()

    def spy(self, spy_role, target):
        self.validate(spy_role, SpyPhase)
        self.check_same_game(spy_role, target)

        spy_role.as_role(Spy).spy(target)

        self.game.proceed()

    def count_mutants(self, computer_scientist):
        self.validate(computer_scientist, ComputerScientistPhase)

        computer_scientist.as_role(ComputerScientist).count_mutants()

        self.game.proceed()

    def next_phase(self, astronaut):
        self.game.phase.mark_ready(astronaut)

        self.game.resolve_phase_if_ready(False)

    def enter_room(self, astronaut, room):
        self.game.enter_room(astronaut, room)

    def room_permission(self, astronaut, room):
        self.game.check_room_free(astronaut, room)

    def mates_statuses(self, astronaut):
        self.game.mates_statuses(astronaut)
