from uuid import uuid4


class Identified:
    def __init__(self):
        self.id = str(uuid4())
