from src.server.exceptions import MissingArgumentError, UserNotLoggedError, NoSuchGameError, \
    GameNotStartedError, GameStartedError


class ArgumentParser:
    def __init__(self, data, server):
        self.data = data
        self.server = server

    def get_arguments(self, *keys, nullable=None):
        args = []
        nullable = [] if nullable is None else nullable
        for key in keys:
            args.append(self.get_argument(key, key in nullable))
        return args

    def get_argument(self, key, nullable=False):
        try:
            return self.data[key]
        except KeyError:
            if nullable:
                return None
            else:
                raise MissingArgumentError

    def get_sender(self):
        session_token = self.get_argument("sessionToken")
        sender = self.server.get_user_from_session_token(session_token)
        if sender is None:
            raise UserNotLoggedError
        return sender

    def get_game(self, started=None):
        game_id = self.get_argument("gameId")
        try:
            game = next(game for game in self.server.games if game.id == game_id)
        except StopIteration:
            raise NoSuchGameError
        if started is not None:
            if started and not game.started:
                raise GameNotStartedError
            if not started and game.started:
                raise GameStartedError
        return game
