class ServerError(Exception):
    pass


class MissingArgumentError(ServerError):
    pass


class InvalidJSonError(ServerError):
    pass


class UserNotLoggedError(ServerError):
    pass


class NoSuchGameError(ServerError):
    pass


class SessionTokenWebsocketMismatchError(ServerError):
    pass


class NoSuchActionError(ServerError):
    pass


class NotTheGameAdminError(ServerError):
    pass


class NotEnoughPlayersError(ServerError):
    pass


class AlreadyInGameError(ServerError):
    pass


class NoSuchUserError(ServerError):
    pass


class GameStartedError(ServerError):
    pass


class GameNotStartedError(ServerError):
    pass


class NoSuchUserInGameError(ServerError):
    pass


class CannotHackThisRoleError(ServerError):
    pass
