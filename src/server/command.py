from src.engine.enums import ROLES
from src.engine.gameprofile import GameProfile
from src.server.argument_parser import ArgumentParser
from src.server.event import Event
from src.server.exceptions import NoSuchActionError, NotTheGameAdminError, NotEnoughPlayersError, UserNotLoggedError, \
    MissingArgumentError
from src.server.game import User, Session, Game

COMMANDS = {}


def action(command_name, game_action=False, target=False):
    def deco(function):
        COMMANDS[command_name] = {
            "func": function,
            "game_action": game_action,
            "target": target
        }

        return function

    return deco


class Commands:
    def __init__(self, json_data, server, websocket):
        self.parser = ArgumentParser(json_data, server)
        self.server = server
        self.websocket = websocket

    def relog_if_necessary(self):
        try:
            sender = self.parser.get_sender()
        except (UserNotLoggedError, MissingArgumentError):
            pass
        else:
            if self.websocket != sender.session.websocket:
                sender.session.websocket = self.websocket

    def execute(self):
        act = self.parser.get_argument("action")
        args = [self]
        kwargs = {}

        self.relog_if_necessary()

        try:
            command = COMMANDS[act]
        except KeyError:
            raise NoSuchActionError
        if command["game_action"]:
            game = self.parser.get_game(started=True)
            kwargs["sender"] = game.get_astronaut(self.parser.get_sender())
            kwargs["game"] = game
            if command["target"]:
                kwargs["target"] = game.get_astronaut_from_id(self.parser.get_argument("targetId"))
        command["func"](*args, **kwargs)

    @action("LOGIN")
    def login(self):
        session_token, name, rank = self.parser.get_arguments("sessionToken", "name", "rank",
                                                              nullable=["sessionToken", "rank"])
        sender = None
        try:
            sender = next(user for user in self.server.users if user.name == name)
        except StopIteration:
            pass
        if sender is None:
            sender = User(name, Session(self.websocket), rank)
            self.server.users.append(sender)
        else:
            sender.session.websocket = self.websocket
            sender.name = name
            sender.rank = rank
        payload = {
            "event": "LOGIN",
            "user": sender.to_json()
        }
        self.server.events.append(Event(self.server.get_logged_users(excepted=sender), payload))
        self.server.events.append(Event([sender], {**payload, **{"sessionToken": sender.session.id}}))

    @action("CREATE_GAME")
    def create_game(self):
        sender = self.parser.get_sender()
        game_name = self.parser.get_argument("gameName")
        initial_roles_number_arg = self.parser.get_argument("initialRolesNumber", nullable=True) or {}
        delayed_psychologist = self.parser.get_argument("delayedPsychologist", nullable=True) or True
        auto_next_phase = self.parser.get_argument("autoNextPhase", nullable=True) or False
        initial_roles_number = {}
        for role in ROLES:
            if (role_name := role.__name__) in initial_roles_number_arg:
                initial_roles_number[role] = initial_roles_number_arg[role_name]
        game = Game(self.server, game_name, sender, GameProfile(initial_roles_number=initial_roles_number,
                                                                delayed_psychologist=delayed_psychologist,
                                                                auto_next_phase=auto_next_phase))
        self.server.games.append(game)
        self.server.events.append(Event(self.server.get_logged_users(), {
            "event": "GAME_CREATED",
            "game": game.to_json()
        }))

    @action("DELETE_GAME")
    def delete_game(self):
        sender = self.parser.get_sender()
        game = self.parser.get_game()
        if sender != game.admin:
            raise NotTheGameAdminError
        self.server.games.remove(game)
        self.server.events.append(Event(self.server.get_logged_users(), {
            "event": "GAME_DELETED",
            "game": game.to_json()
        }))

    @action("JOIN_GAME")
    def join_game(self):
        sender = self.parser.get_sender()
        game = self.parser.get_game(started=False)
        as_operator = self.parser.get_argument("asOperator", nullable=True)
        game.add_user(sender, as_operator=as_operator)
        self.server.events.append(Event(self.server.get_logged_users(), {
            "event": "JOINED_GAME",
            "game": game.to_json(),
            "user": sender.to_json(),
            "asOperator": as_operator
        }))

    @action("START_GAME")
    def start_game(self):
        sender = self.parser.get_sender()
        game = self.parser.get_game()
        if sender != game.admin:
            raise NotTheGameAdminError
        if len(game.users) < game.game_profile.min_initial_astronaut_number:
            raise NotEnoughPlayersError
        game.start()

    @action("VOTE_MUTATE", game_action=True, target=True)
    def vote_mutate(self, sender, target, game):
        game.game_engine.interface.vote_mutate(sender, target)

    @action("VOTE_MUTANT_KILL", game_action=True, target=True)
    def vote_mutant_kill(self, sender, target, game):
        game.game_engine.interface.vote_mutant_kill(sender, target)

    @action("VOTE_PARALYSE", game_action=True, target=True)
    def vote_paralyse(self, sender, target, game):
        game.game_engine.interface.vote_paralyse(sender, target)

    @action("VOTE_BRAINWASH", game_action=True, target=True)
    def vote_brainwash(self, sender, target, game):
        game.game_engine.interface.vote_brainwash(sender, target)

    @action("VOTE_DOCTOR_KILL", game_action=True, target=True)
    def vote_doctor_kill(self, sender, target, game):
        game.game_engine.interface.vote_doctor_kill(sender, target)

    @action("HEAL", game_action=True, target=True)
    def vote_heal(self, sender, target, game):
        game.game_engine.interface.heal(sender, target)

    @action("SELECT_HEAL", game_action=True, target=True)
    def select_heal(self, sender, target, game):
        game.game_engine.interface.select_heal(sender, target)

    @action("SELECT_DOCTOR_KILL", game_action=True, target=True)
    def select_doctor_kill(self, sender, target, game):
        game.game_engine.interface.select_doctor_kill(sender, target)

    @action("SELECT_MUTATE", game_action=True, target=True)
    def select_mutate(self, sender, target, game):
        game.game_engine.interface.select_mutate(sender, target)

    @action("SELECT_MUTANT_KILL", game_action=True, target=True)
    def select_mutant_kill(self, sender, target, game):
        game.game_engine.interface.select_mutant_kill(sender, target)

    @action("SELECT_BRAINWASH", game_action=True, target=True)
    def select_brainwash(self, sender, target, game):
        game.game_engine.interface.select_brainwash(sender, target)

    @action("SELECT_PARALYSE", game_action=True, target=True)
    def select_paralyse(self, sender, target, game):
        game.game_engine.interface.select_paralyse(sender, target)

    @action("VOTE_EXECUTION", game_action=True)
    def vote_execution(self, sender, game):
        # the decorator target option is not used because the target_id can be None
        target_id = self.parser.get_argument("targetId")
        vote_target = None
        if target_id is not None:
            vote_target = game.get_astronaut_from_id(target_id)
        game.game_engine.interface.vote_execution(sender, vote_target)

    @action("PSYCHOLOGIST_ANALYSE", game_action=True, target=True)
    def psychologist_analyse(self, sender, target, game):
        game.game_engine.interface.psychologist_analysis(sender, target)

    @action("GENETICIST_ANALYSE", game_action=True, target=True)
    def geneticist_analyse(self, sender, target, game):
        game.game_engine.interface.geneticist_analysis(sender, target)

    @action("HACK", game_action=True)
    def hack(self, sender, game):
        hacked_role = self.parser.get_argument("hackedRole")
        game.game_engine.interface.hack(sender, hacked_role)

    @action("INDIC_INVESTIGATE", game_action=True, target=True)
    def indic_investigation(self, sender, target, game):
        game.game_engine.interface.indic_investigation(sender, target)

    @action("INVESTIGATE", game_action=True, target=True)
    def investigate(self, sender, target, game):
        game.game_engine.interface.investigate(sender, target)

    @action("SPY", game_action=True, target=True)
    def spy(self, sender, target, game):
        game.game_engine.interface.spy(sender, target)

    @action("COUNT_MUTANTS", game_action=True)
    def count_mutants(self, sender, game):
        game.game_engine.interface.count_mutants(sender)

    @action("NEXT_PHASE", game_action=True)
    def next_phase(self, sender, game):
        game.game_engine.interface.next_phase(sender)

    @action("GET_LOBBY")
    def get_lobby(self):
        sender = self.parser.get_sender()
        self.server.events.append(Event([sender], {
            "event": "LOBBY",
            "games": [game.to_json() for game in self.server.games],
            "users": [user.to_json() for user in self.server.get_logged_users()],
        }))

    @staticmethod
    def ast_to_json(ast, game, public_info_only):
        json = ast.to_json(public_info_only=public_info_only)
        if not public_info_only:
            reports = ast.night_reports
            formatted_actions = []
            if reports is not None:
                for report in reports:
                    convert_event = game.convert_event(report)
                    del convert_event["recipients"]
                    del convert_event["game"]
                    formatted_actions.append(convert_event)
            json["night_actions"] = formatted_actions
        return json

    @action("GET_GAME")
    def get_game(self):
        game = self.parser.get_game(started=True)
        sender = self.parser.get_sender()

        data = {
            "event": "GAME",
            "astronauts": {
                user.id: self.ast_to_json(
                    ast, game,
                    public_info_only=False if sender.id == user.id or sender in game.operators else True)
                for user, ast in game.user_astronaut_mapping.items()},
            "phase": game.game_engine.phase.to_json(),
            "game": game.to_json(),
            "roundNumber": game.game_engine.round_number
        }

        self.server.events.append(Event([sender], data))

    @action("SAVE")
    def save(self):
        print("saving")
        self.server.save("save_file")

    @action("ROOM_ENTER", game_action=True)
    def enter_room(self, sender, game):
        room_name = self.parser.get_argument("roomName")
        room = game.game_engine.get_room(room_name)
        game.game_engine.interface.enter_room(sender, room)

    @action("ROOM_PERMISSION", game_action=True)
    def room_permission(self, sender, game):
        room_name = self.parser.get_argument("roomName")
        room = game.game_engine.get_room(room_name)
        game.game_engine.interface.room_permission(sender, room)

    @action("MATES_STATUSES", game_action=True)
    def mates_statuses(self, sender, game):
        game.game_engine.interface.mates_statuses(sender)

    @action("END_EXECUTION_VOTES")
    def end_execution_votes(self):
        game = self.parser.get_game(started=True)
        game.game_engine.interface.end_execution_votes()
