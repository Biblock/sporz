import json

import websockets

from src.server.identified import Identified


class EventRecipient(Identified):
    def personalize_payload(self, payload):
        raise NotImplementedError

    @property
    def websocket(self):
        raise NotImplementedError

    @property
    def logged(self):
        return self.websocket is not None


class WSEventRecipient(EventRecipient):
    def __init__(self, websocket):
        super().__init__()
        self.ws = websocket
        self.id = f"WSRecipient_{self.id}"

    def personalize_payload(self, payload):
        pass

    @property
    def websocket(self):
        return self.ws


class Event:
    def __init__(self, recipients, payload):
        self.recipients = recipients
        self.payload = payload

    async def send(self):
        for recipient in self.recipients:
            if not recipient.logged:
                continue
            pl = self.payload.copy()
            recipient.personalize_payload(pl)
            try:
                websocket = recipient.websocket
                await websocket.send(json.dumps(pl))
            except websockets.ConnectionClosed:
                print(f"Connection closed {recipient.id}")
                # todo log
            except AttributeError:
                print(f"AttributeError {recipient.id}. This should never happen...")

    def __repr__(self):
        return json.dumps(self.payload)
