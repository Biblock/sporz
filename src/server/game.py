import random
from collections.abc import Iterable
from dataclasses import fields

from src.engine.astronaut import Astronaut
from src.engine.game_engine import GameEngine
from src.engine.gameprofile import GameProfile
from src.server.event import Event, EventRecipient
from src.server.exceptions import AlreadyInGameError, NoSuchUserInGameError
from src.server.identified import Identified


class JSONAble:
    def to_json(self):
        raise NotImplementedError


class Session(Identified):
    def __init__(self, websocket):
        super().__init__()
        self.websocket = websocket


class GameObserver:
    def add_event(self, event):
        raise NotImplementedError


class Game(Identified, JSONAble, GameObserver):
    def __init__(self, server, name, admin, game_profile=None):
        super().__init__()
        self.server = server
        self.user_astronaut_mapping = {}
        self.name = name
        self.admin = admin
        self.game_engine = None
        self.game_profile = GameProfile() if game_profile is None else game_profile
        self.operators = []

    @property
    def users(self):
        return self.user_astronaut_mapping.keys()

    @property
    def started(self):
        return self.game_engine is not None

    def get_astronaut(self, user):
        try:
            return self.user_astronaut_mapping[user]
        except KeyError:
            raise NoSuchUserInGameError

    def get_astronaut_from_id(self, user_id):
        try:
            user = next(u for u in self.users if u.id == user_id)
            return self.get_astronaut(user)
        except StopIteration:
            raise NoSuchUserInGameError

    def add_user(self, user, as_operator=False):
        if not as_operator:
            if user in self.users:
                raise AlreadyInGameError
            if not self.started:
                self.user_astronaut_mapping[user] = None
        else:
            if user in self.operators:
                raise AlreadyInGameError
            self.operators.append(user)

    def start(self):
        self.game_engine = GameEngine(self, self.game_profile, len(self.users))
        self.server.events.append(Event(self.server.get_logged_users(), {
            "event": "GAME_STARTED",
            "game": self.to_json(),
            "users": [u.to_json() for u in self.users]
        }))
        astronauts = self.game_engine.astronauts.copy()
        random.shuffle(astronauts)
        for key in self.user_astronaut_mapping:
            self.user_astronaut_mapping[key] = astronauts.pop(0)

        self.game_engine.start()

    def get_user_from_astronaut(self, astronaut):
        return next(user for user, astro in self.user_astronaut_mapping.items() if astro == astronaut)

    def add_event(self, event):
        converted_event = self.convert_event(event)
        recipients = [*converted_event["recipients"], *self.operators]
        payload = {key: value for key, value in converted_event.items() if key != "recipients"}
        self.server.events.append(Event(recipients, payload))

    @property
    def events(self):
        return self.server.events

    def convert_event(self, event):
        converted_event = {
            field.name: self.convert_astronauts(
                getattr(event, field.name), False if field.name == "recipients" else True) for field in fields(event)}
        converted_event["event"] = event.get_name()
        converted_event["game"] = self.to_json()
        converted_event["roundNumber"] = self.game_engine.round_number
        return converted_event

    def convert_astronauts(self, item, to_json=False):
        if isinstance(item, Iterable) and not isinstance(item, (str, dict)):
            return [self.convert_astronauts(i, to_json) for i in item]
        if type(item) is Astronaut:
            astro = self.get_user_from_astronaut(item)
            return astro.to_json() if to_json else astro
        return item

    def to_json(self):
        return {
            "name": self.name,
            "id": self.id,
            "admin": self.admin.to_json(),
            "minNumberOfPlayer": self.game_profile.min_initial_astronaut_number,
            "users": [user.to_json() for user in self.users],
            "started": self.started,
            "operators": [op.to_json() for op in self.operators]
        }


class User(JSONAble, EventRecipient):
    def __init__(self, name, session, rank=None):
        super().__init__()
        self.name = name
        self.session = session
        self.rank = rank

    def to_json(self):
        return {
            "id": self.id,
            "name": self.name,
            "rank": self.rank
        }

    def personalize_payload(self, payload):
        payload["recipientId"] = self.id

    @property
    def websocket(self):
        return self.session.websocket
