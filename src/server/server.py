import asyncio
import json
import os
import pickle
from datetime import datetime

import websockets

from src.engine.exceptions import EngineError
from src.server.command import Commands
from src.server.event import Event, WSEventRecipient
from src.server.exceptions import ServerError, InvalidJSonError


class Server:
    def __init__(self, port, loop, saves_folder=None, use_save=None):
        self.port = port
        self.event_loop = loop
        self.saves_folder = saves_folder
        self.users = []
        self.events = []
        self.games = []

        failed_to_load_save = False
        if use_save is not None:
            try:
                save_file_path = os.path.join(saves_folder, use_save)
                with open(save_file_path, "rb") as file:
                    self.games = pickle.load(file)
            except pickle.UnpicklingError as e:
                print("Failed to load save", use_save)
                print(repr(e))
                failed_to_load_save = True
            else:
                for game in self.games:
                    game.game_engine.observer = game
                    game.server = self

                self.users = list(user for g in self.games for user in g.users)

        if use_save is None or failed_to_load_save:
            self.games = []

    def start(self):
        # noinspection PyTypeChecker
        start_server = websockets.serve(self.main_loop, "0.0.0.0", self.port)
        self.event_loop.run_until_complete(start_server)

    def logout(self, websocket):
        login_out_users = [user for user in self.users if user.session.websocket == websocket]
        for u in login_out_users:
            u.session.websocket = None
        logged_users = self.get_logged_users()
        for user in login_out_users:
            self.events.append(Event(logged_users, {
                "event": "LOGOUT",
                "user": user.to_json()
            }))

    async def main_loop(self, websocket, _):
        lock = asyncio.Lock()
        try:
            print("new_socket")
            async for message in websocket:
                async with lock:
                    print("message")
                    self.process_message(websocket, message)
                    self.save()
                    await self.send_events()
        finally:
            print("logout")
            self.logout(websocket)
            await self.send_events()

    async def send_events(self):
        for event in self.events:
            await event.send()
        self.events = []

    def process_message(self, websocket, message):
        try:
            try:
                json_data = json.loads(message)
            except json.JSONDecodeError:
                raise InvalidJSonError
            print(json_data)
            command = Commands(json_data, self, websocket)
            command.execute()
        except (EngineError, ServerError) as e:
            try:
                recipient = command.parser.get_sender()
            except Exception:
                recipient = WSEventRecipient(websocket)
            self.events.append(Event([recipient], {
                "error": type(e).__name__
            }))

    def get_user_from_session_token(self, session_token):
        try:
            return next(u for u in self.users if u.session.id == session_token)
        except StopIteration:
            return None

    def get_logged_users(self, excepted=None):
        return [u for u in self.users if u.logged and (excepted is None or u.id != excepted.id)]

    def save(self, file_name=None):
        if file_name is None:
            current_date = datetime.now().strftime("%Y%m%d_%H%M%S")
            file_name = f"{current_date}.save"
        file_path = os.path.join(self.saves_folder, file_name)

        os.makedirs(os.path.dirname(file_path), exist_ok=True)
        with open(file_path, "wb") as file:
            users_bk = {}
            games_bk = {}

            for user in self.users:
                users_bk[user] = user.session.websocket
                user.session.websocket = None
            for game in self.games:
                games_bk[game] = {"observer": None if game.game_engine is None else game.game_engine.observer,
                                  "server": game.server}
                if game.game_engine is not None:
                    game.game_engine.observer = None
                game.server = None
            try:
                pickle.dump(self.games, file)
            except Exception as e:
                print(repr(e))
                raise e

            for user in self.users:
                user.session.websocket = users_bk[user]
            for game in self.games:
                if game.game_engine is not None:
                    game.game_engine.observer = games_bk[game]["observer"]
                game.server = games_bk[game]["server"]
