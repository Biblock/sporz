from src.engine.roles.computer_scientist import ComputerScientist
from src.engine.roles.doctor import Doctor
from src.engine.roles.geneticist import Geneticist
from src.engine.roles.hacker import Hacker
from src.engine.roles.indic import Indic
from src.engine.roles.investigator import Investigator
from src.engine.roles.mutant import Mutant
from src.engine.roles.psychologist import Psychologist
from src.engine.roles.spy import Spy
from src.engine.roles.traitor import Traitor


def test_full_game(scenario_helper):
    h = scenario_helper
    game = h.game
    cmd = h.cmd
    cmd_game_act = cmd
    game.game_profile.auto_next_phase = True

    mutant = h.get_role_user(Mutant)
    indic = h.get_role_user(Indic)
    psychologist = h.get_role_user(Psychologist)
    traitor = h.get_role_user(Traitor)
    doctor_1, doctor_2 = (u for u in game.users if game.get_astronaut(u).has_role(Doctor))
    geneticist = h.get_role_user(Geneticist)
    hacker = h.get_role_user(Hacker)
    investigator = h.get_role_user(Investigator)
    spy = h.get_role_user(Spy)
    computer_scientist = h.get_role_user(ComputerScientist)
    astronaut = next(u for u in game.users if not game.get_astronaut(u).roles)

    assert game.game_engine.round_number == 1
    cmd_game_act(mutant, {
        "action": "VOTE_MUTATE",
        "targetId": astronaut.id
    })

    assert game.game_engine.round_number == 1
    cmd_game_act(mutant, {
        "action": "VOTE_BRAINWASH",
        "targetId": traitor.id
    })

    cmd_game_act(doctor_1, {
        "action": "HEAL",
        "targetId": psychologist.id
    })

    cmd_game_act(doctor_2, {
        "action": "HEAL",
        "targetId": mutant.id
    })

    cmd_game_act(psychologist, {
        "action": "PSYCHOLOGIST_ANALYSE",
        "targetId": traitor.id
    })

    cmd_game_act(computer_scientist, {
        "action": "COUNT_MUTANTS"
    })

    cmd_game_act(geneticist, {
        "action": "GENETICIST_ANALYSE",
        "targetId": mutant.id
    })

    cmd_game_act(hacker, {
        "action": "HACK",
        "hackedRole": "PSYCHOLOGIST"
    })

    cmd_game_act(indic, {
        "action": "INDIC_INVESTIGATE",
        "targetId": traitor.id
    })

    cmd_game_act(spy, {
        "action": "SPY",
        "targetId": traitor.id
    })

    cmd(geneticist, {
        "action": "VOTE_EXECUTION",
        "targetId": mutant.id
    })
    cmd(mutant, {
        "action": "VOTE_EXECUTION",
        "targetId": indic.id
    })
    white_votes = [traitor, computer_scientist, hacker, doctor_1, doctor_2, geneticist, indic, psychologist,
                   investigator, spy, astronaut]

    assert game.game_engine.round_number == 1
    h.set_nb_event_checkpoint()
    for voter in white_votes:
        cmd(voter, {
            "action": "VOTE_EXECUTION",
            "targetId": None
        })

    last_events = h.get_events_after_checkpoint()
    assert game.game_engine.round_number == 2
    assert game.game_engine.phase.ACTIVE_ROLE == Mutant
    phase_over = [e for e in last_events if e.payload["event"] == "PHASE_OVER"]
    phase_over_vote_result = [e for e in phase_over if e.payload["phase"]["name"] == "NEW_DAY"]
    assert len(phase_over) == 1
    assert len(phase_over_vote_result) == 1
    # End of the first round
