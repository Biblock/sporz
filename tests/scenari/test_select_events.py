import pytest

from src.engine.exceptions import UnauthorizedRoleError
from src.engine.phase import FirstMutantPhase, SecondMutantPhase, PsychologistPhase
from src.engine.roles.computer_scientist import ComputerScientist
from src.engine.roles.doctor import Doctor
from src.engine.roles.mutant import Mutant
from src.engine.roles.psychologist import Psychologist


def send_select_action(helper, select_command, selector, target):
    helper.cmd(selector, {
        "action": select_command,
        "targetId": target.id
    })


def check_successful_select_action(helper, select_command, selector, target, recipients):
    send_select_action(helper, select_command, selector, target)
    assert helper.last_event.payload["event"] == select_command
    assert helper.last_event.payload["voter"]["id"] == selector.id
    assert helper.last_event.payload["target"]["id"] == target.id
    actual_recipients = set(helper.last_event.recipients)
    assert actual_recipients == recipients


def check_select_action(helper, select_command, selector, target, bad_selector, recipients):
    with pytest.raises(UnauthorizedRoleError):
        send_select_action(helper, select_command, bad_selector, target)

    for r in recipients:
        # Shows that you can select yourself and your kind
        check_successful_select_action(helper, select_command, selector, target=r, recipients=recipients)
    check_successful_select_action(helper, select_command, selector, target, recipients)


def setup_psychologist_phase(game, mutant, computer_scientist, doctor_1, doctor_2, psychologist):
    astof = game.get_astronaut
    game.game_engine.interface.vote_mutate(astof(mutant), astof(computer_scientist))
    game.game_engine.interface.next_phase(astof(mutant))
    game.game_engine.interface.vote_paralyse(astof(mutant), astof(computer_scientist))
    game.game_engine.interface.next_phase(astof(mutant))
    game.game_engine.interface.heal(astof(doctor_1), astof(computer_scientist))
    game.game_engine.interface.next_phase(astof(doctor_1))
    game.game_engine.interface.heal(astof(doctor_2), astof(computer_scientist))
    game.game_engine.interface.next_phase(astof(doctor_2))
    game.game_engine.interface.psychologist_analysis(astof(psychologist), astof(computer_scientist))


def test_select_methods(scenario_helper):
    h = scenario_helper
    game = h.game
    cmd = h.cmd
    cmd_game_act = h.cmd_game_act

    mutant = h.get_role_user(Mutant)
    computer_scientist = h.get_role_user(ComputerScientist)
    psychologist = h.get_role_user(Psychologist)
    astronaut = next(u for u in game.users if not game.get_astronaut(u).roles)
    doctor_1, doctor_2 = (u for u in game.users if game.get_astronaut(u).has_role(Doctor))

    assert game.game_engine.phase.ACTIVE_ROLE == Mutant
    assert not game.game_engine.phase.is_over
    setup_psychologist_phase(game, mutant, computer_scientist, doctor_1, doctor_2, psychologist)

    assert game.game_engine.phase.ACTIVE_ROLE == Psychologist
    assert game.game_engine.phase.is_over

    check_select_action(h, "SELECT_MUTATE", mutant, astronaut, bad_selector=astronaut, recipients={mutant})
    check_select_action(h, "SELECT_MUTATE", mutant, astronaut, bad_selector=doctor_1, recipients={mutant})

    assert game.game_engine.phase.ACTIVE_ROLE == Psychologist  # Shows that the phase doesn't have to be a Doctor's phase
    check_select_action(h, "SELECT_HEAL", doctor_1, astronaut, bad_selector=astronaut, recipients={doctor_1, doctor_2})
    check_select_action(h, "SELECT_DOCTOR_KILL", doctor_1, astronaut, bad_selector=astronaut,
                        recipients={doctor_1, doctor_2})
    check_select_action(h, "SELECT_MUTANT_KILL", mutant, astronaut, bad_selector=astronaut, recipients={mutant})
    check_select_action(h, "SELECT_BRAINWASH", mutant, astronaut, bad_selector=astronaut, recipients={mutant})
    check_select_action(h, "SELECT_PARALYSE", mutant, astronaut, bad_selector=astronaut, recipients={mutant})

    assert type(game.game_engine.phase) is PsychologistPhase
    assert game.game_engine.phase.is_over
    assert not game.game_engine.phase.all_next_phase_ready()

    psycho_phase_over = [e for e in game.events if
                         e.payload["event"] == "PHASE_OVER" and e.payload["phase"]["name"] == "PSYCHOLOGIST_PHASE"]
    assert len(psycho_phase_over) == 1
