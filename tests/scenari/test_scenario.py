import pytest

from src.engine.roles.computer_scientist import ComputerScientist
from src.engine.roles.doctor import Doctor
from src.engine.roles.geneticist import Geneticist
from src.engine.roles.hacker import Hacker
from src.engine.roles.indic import Indic
from src.engine.roles.investigator import Investigator
from src.engine.roles.mutant import Mutant
from src.engine.roles.psychologist import Psychologist
from src.engine.roles.spy import Spy
from src.engine.roles.traitor import Traitor


@pytest.mark.parametrize("psycho_target_round_2, mutant_expectation",
                         [("astro", True), ("mutant", True), ("psychologist", False), ("geneticist", False),
                          ("traitor", True)])
def test_full_game(scenario_helper, psycho_target_round_2, mutant_expectation):
    h = scenario_helper
    game = h.game
    cmd = h.cmd
    cmd_game_act = h.cmd_game_act

    mutant = h.get_role_user(Mutant)
    indic = h.get_role_user(Indic)
    psychologist = h.get_role_user(Psychologist)
    traitor = h.get_role_user(Traitor)
    doctor_1, doctor_2 = (u for u in game.users if game.get_astronaut(u).has_role(Doctor))
    geneticist = h.get_role_user(Geneticist)
    hacker = h.get_role_user(Hacker)
    investigator = h.get_role_user(Investigator)
    spy = h.get_role_user(Spy)
    computer_scientist = h.get_role_user(ComputerScientist)
    astronaut = next(u for u in game.users if not game.get_astronaut(u).roles)

    assert game.game_engine.round_number == 1
    h.events_contain_event(h.get_events_after_checkpoint(True), "MATES_STATUSES", recipient=[mutant], occurrence=1)
    cmd_game_act(mutant, {
        "action": "VOTE_MUTATE",
        "targetId": astronaut.id
    })
    h.events_contain_event(h.get_events_after_checkpoint(False), "VOTE_MUTATE", recipient=[mutant], target=astronaut)
    h.events_contain_event(h.get_events_after_checkpoint(False), "MUTATION", recipient=[astronaut], target=astronaut)
    mates_statuses = h.events_contain_event(h.get_events_after_checkpoint(False), "MATES_STATUSES", recipient=[mutant])
    assert set(a["id"] for a in mates_statuses[0].payload["disabled"]) == set([astronaut.id])
    assert set(a["id"] for a in mates_statuses[0].payload["not_disabled"]) == set([mutant.id])
    assert mates_statuses[0].payload["role"] == "Mutant"
    h.set_nb_event_checkpoint()

    assert game.game_engine.round_number == 1
    cmd_game_act(mutant, {
        "action": "VOTE_BRAINWASH",
        "targetId": traitor.id
    })
    h.events_contain_event(h.get_events_after_checkpoint(False), "VOTE_BRAIN_WASH",
                           recipient=[mutant, astronaut], target=traitor)
    h.events_contain_event(h.get_events_after_checkpoint(False), "MUTANT_BRAIN_WASHED",
                           recipient=[traitor], target=traitor)
    h.set_nb_event_checkpoint()

    cmd(astronaut, {
        "action": "MATES_STATUSES"
    })
    mates_statuses = h.events_contain_event(h.get_events_after_checkpoint(False), "MATES_STATUSES",
                                            recipient=[astronaut])
    assert set(a["id"] for a in mates_statuses[0].payload["disabled"]) == set([astronaut.id])
    assert set(a["id"] for a in mates_statuses[0].payload["not_disabled"]) == set([mutant.id])
    assert mates_statuses[0].payload["role"] == "Mutant"

    cmd_game_act(doctor_1, {
        "action": "HEAL",
        "targetId": spy.id
    })

    # important, people know doctors tried to heal them even if they were not mutated
    h.events_contain_event(h.get_events_after_checkpoint(), "HEALED", recipient=[spy], target=spy)

    cmd_game_act(doctor_2, {
        "action": "HEAL",
        "targetId": mutant.id
    })

    # Because host
    h.events_contain_event(h.get_events_after_checkpoint(), "RESISTED_HEALING", recipient=[mutant], target=mutant)

    cmd_game_act(psychologist, {
        "action": "PSYCHOLOGIST_ANALYSE",
        "targetId": traitor.id
    })

    report = h.events_contain_event(h.get_events_after_checkpoint(), "PSYCHOLOGIST_ANALYSIS",
                                    recipient=[psychologist], target=traitor)[0]
    assert not report.payload["mutant"]  # even if brainwashed it will have effect the next round

    cmd_game_act(computer_scientist, {
        "action": "COUNT_MUTANTS"
    })

    report = h.events_contain_event(h.get_events_after_checkpoint(), "COMPUTER_SCIENTIST_REPORT",
                                    recipient=[computer_scientist])[0]
    assert report.payload["mutant_number"] == 2

    cmd_game_act(geneticist, {
        "action": "GENETICIST_ANALYSE",
        "targetId": mutant.id
    })
    report = h.events_contain_event(h.get_events_after_checkpoint(), "GENETICIST_ANALYSIS",
                                    recipient=[geneticist])[0]
    assert report.payload["genome"] == "HOST"

    cmd_game_act(hacker, {
        "action": "HACK",
        "hackedRole": "PSYCHOLOGIST"
    })

    report = h.events_contain_event(h.get_events_after_checkpoint(), "PSYCHOLOGIST_ANALYSIS",
                                    recipient=[hacker], target=traitor)[0]
    assert not report.payload["mutant"]  # even if brainwashed it will have effect the next round

    cmd(indic, {
        "action": "GET_GAME"
    })
    game_event = h.events_contain_event(h.get_events_after_checkpoint(), "GAME")[0]
    night_actions = game_event.payload["astronauts"][indic.id]["night_actions"]
    assert len(night_actions) == 0

    cmd_game_act(indic, {
        "action": "INDIC_INVESTIGATE",
        "targetId": traitor.id
    })

    report = h.events_contain_event(h.get_events_after_checkpoint(), "INDIC_REPORT",
                                    recipient=[indic], target=traitor)[0]
    assert report.payload["traitor"]

    cmd(indic, {
        "action": "GET_GAME",
    })
    game_event = h.events_contain_event(h.get_events_after_checkpoint(), "GAME")[0]
    night_actions = game_event.payload["astronauts"][indic.id]["night_actions"]
    assert len(night_actions) == 1
    assert night_actions[0]["traitor"]

    cmd_game_act(spy, {
        "action": "SPY",
        "targetId": traitor.id
    })

    report = h.events_contain_event(h.get_events_after_checkpoint(), "SPY_REPORT",
                                    recipient=[spy], target=traitor)[0]
    assert report.payload["brain_washed"]
    assert report.payload["psychologist_analysed"]
    assert not report.payload["geneticist_analysed"]


    cmd(indic, {
        "action": "GET_GAME",
    })
    game_event = h.events_contain_event(h.get_events_after_checkpoint(), "GAME")[0]
    night_actions = game_event.payload["astronauts"][indic.id]["night_actions"]
    assert len(night_actions) == 1
    assert night_actions[0]["traitor"]

    cmd(geneticist, {
        "action": "VOTE_EXECUTION",
        "targetId": mutant.id
    })
    cmd(mutant, {
        "action": "VOTE_EXECUTION",
        "targetId": indic.id
    })
    white_votes = [traitor, computer_scientist, hacker, doctor_1, doctor_2, geneticist, indic, psychologist,
                   investigator, spy, astronaut]

    assert game.game_engine.round_number == 1
    h.set_nb_event_checkpoint()
    for voter in white_votes:
        cmd(voter, {
            "action": "VOTE_EXECUTION",
            "targetId": None
        })

    last_events = h.get_events_after_checkpoint()
    assert game.game_engine.round_number == 2
    assert game.game_engine.phase.ACTIVE_ROLE == Mutant
    phase_over = [e for e in last_events if e.payload["event"] == "PHASE_OVER"]
    phase_over_vote_result = [e for e in phase_over if e.payload["phase"]["name"] == "NEW_DAY"]
    assert len(phase_over) == 1
    assert len(phase_over_vote_result) == 1
    # End of the first round

    cmd(astronaut, {
        "action": "MATES_STATUSES"
    })
    mates_statuses = h.events_contain_event(h.get_events_after_checkpoint(False), "MATES_STATUSES",
                                            recipient=[astronaut])
    assert set(a["id"] for a in mates_statuses[0].payload["not_disabled"]) == set([astronaut.id, mutant.id])
    assert set(a["id"] for a in mates_statuses[0].payload["disabled"]) == set()
    assert mates_statuses[0].payload["role"] == "Mutant"

    cmd_game_act(mutant, {
        "action": "VOTE_MUTANT_KILL",
        "targetId": computer_scientist.id
    })

    cmd_game_act(astronaut, {
        "action": "VOTE_MUTANT_KILL",
        "targetId": psychologist.id
    })

    cmd_game_act(astronaut, {
        "action": "VOTE_MUTANT_KILL",
        "targetId": computer_scientist.id
    })

    h.events_contain_phase(h.get_events_after_checkpoint(), "SECOND_MUTANT_PHASE")

    cmd_game_act(astronaut, {
        "action": "VOTE_PARALYSE",
        "targetId": doctor_1.id
    })

    cmd_game_act(mutant, {
        "action": "VOTE_PARALYSE",
        "targetId": psychologist.id
    })

    cmd_game_act(mutant, {
        "action": "VOTE_PARALYSE",
        "targetId": doctor_1.id
    })

    h.events_contain_phase(h.get_events_after_checkpoint(), "DOCTOR_PHASE")

    cmd_game_act(doctor_2, {
        "action": "HEAL",
        "targetId": astronaut.id
    })

    h.events_contain_phase(h.get_events_after_checkpoint(), "PSYCHOLOGIST_PHASE")

    ################################################################
    target = None
    if psycho_target_round_2 == "astro":
        target = astronaut
    elif psycho_target_round_2 == "mutant":
        target = mutant
    elif psycho_target_round_2 == "psychologist":
        target = psychologist
    elif psycho_target_round_2 == "geneticist":
        target = geneticist
    elif psycho_target_round_2 == "traitor":
        target = traitor
        assert not game.get_astronaut(traitor).has_role(Mutant)
        assert mutant_expectation
    assert target is not None

    cmd_game_act(psychologist, {
        "action": "PSYCHOLOGIST_ANALYSE",
        "targetId": target.id
    })

    h.events_contain_phase(h.get_events_after_checkpoint(False), "GENETICIST_PHASE")
    report = h.events_contain_event(h.get_events_after_checkpoint(False),
                                    "PSYCHOLOGIST_ANALYSIS", recipient=[psychologist], target=target)[0].payload
    h.set_nb_event_checkpoint()
    assert report["mutant"] is mutant_expectation

    ################################################################
