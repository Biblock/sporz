import pytest

from src.engine.astronaut import Genome
from src.engine.roles.hacker import Hacker
from src.engine.roles.indic import Indic
from src.engine.roles.mutant import Mutant
from src.engine.roles.psychologist import Psychologist
from src.server.command import Commands
from src.server.game import Session
from tests.server.test_login import FakeWebsocket
from tests.server.test_start_game import NAMES


def un_randomize_stuff(game):
    for ast in game.astronauts:
        if ast.has_role(Psychologist) or ast.has_role(Mutant):
            ast.genome = Genome.HOST
        elif ast.has_role(Hacker) or ast.has_role(Indic):
            ast.genome = Genome.RESISTANT
        else:
            ast.genome = Genome.NEUTRAL


class FakeUser:
    def __init__(self, websocket):
        self.session = Session(websocket)


class Helper:
    def __init__(self, server):
        self.server = server
        self.game = None
        self.event_checkpoint = 0

    def cmd(self, user, payload, ignore_session_token=False):
        if not ignore_session_token:
            payload["sessionToken"] = user.session.id
        try:
            payload["gameId"] = self.game.id
        except (NameError, AttributeError):
            pass
        Commands(payload, self.server, user.session.websocket).execute()

    def cmd_game_act(self, user, payload):
        self.cmd(user, payload)
        self.cmd(user, {
            "action": "NEXT_PHASE"
        })

    def get_role_user(self, role):
        return next(u for u in self.game.users if self.game.get_astronaut(u).has_role(role))

    @property
    def last_event(self):
        return self.game.events[-1]

    def set_nb_event_checkpoint(self):
        self.event_checkpoint = len(self.game.events)

    def get_events_after_checkpoint(self, set_checkpoint=True):
        delta_events = self.event_checkpoint - len(self.game.events)
        events = self.game.events[delta_events:] if delta_events != 0 else []
        if set_checkpoint:
            self.set_nb_event_checkpoint()
        return events

    @staticmethod
    def events_contain_event(events, event_name, occurrence=1, recipient=None, target=None):
        found = [e for e in events if e.payload["event"] == event_name
                 and (target is None or ("target" in e.payload and e.payload["target"]["id"] == target.id))
                 and (recipient is None or {r for r in e.recipients} == set(recipient))]
        assert len(found) == occurrence
        return found

    def events_contain_phase(self, events, phase_name, occurrence=1):
        events = [e for e in events if e.payload["event"] == "NEW_PHASE" and e.payload["phase"]["name"] == phase_name]
        assert len(events) == occurrence
        assert str(self.game.game_engine.phase) == phase_name
        return events


def log_all(helper, names, shared_websocket):
    for name in names:
        helper.cmd(FakeUser(shared_websocket), {
            "action": "LOGIN",
            "name": name
        })


def setup_game(helper, initial_roles_number=None):
    server = helper.server
    aurore = next(u for u in server.users if u.name == "Aurore")
    game_settings = {
        "action": "CREATE_GAME",
        "gameName": "Sporz"
    }
    if initial_roles_number is not None:
        game_settings["initialRolesNumber"] = initial_roles_number
    helper.cmd(aurore, game_settings)

    game = server.games[0]
    helper.game = game

    for u in server.users:
        helper.cmd(u, {
            "action": "JOIN_GAME",
        })
    helper.cmd(aurore, {
        "action": "START_GAME",
    })
    un_randomize_stuff(game.game_engine)


@pytest.fixture
def scenario_helper(server):
    websocket = FakeWebsocket()
    h = Helper(server)

    log_all(h, NAMES[:12], websocket)
    setup_game(h, None)
    return h
