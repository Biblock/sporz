import pytest

from src.engine.roles.computer_scientist import ComputerScientist
from src.engine.roles.doctor import Doctor
from src.engine.roles.geneticist import Geneticist
from src.engine.roles.hacker import Hacker
from src.engine.roles.indic import Indic
from src.engine.roles.investigator import Investigator
from src.engine.roles.mutant import Mutant
from src.engine.roles.psychologist import Psychologist
from src.engine.roles.spy import Spy
from src.engine.roles.traitor import Traitor


@pytest.mark.parametrize("execute_someone", [True, False])
def test_end_votes(scenario_helper, execute_someone):
    h = scenario_helper
    game = h.game
    cmd = h.cmd
    cmd_game_act = h.cmd_game_act

    mutant = h.get_role_user(Mutant)
    indic = h.get_role_user(Indic)
    psychologist = h.get_role_user(Psychologist)
    traitor = h.get_role_user(Traitor)
    doctor_1, doctor_2 = (u for u in game.users if game.get_astronaut(u).has_role(Doctor))
    geneticist = h.get_role_user(Geneticist)
    hacker = h.get_role_user(Hacker)
    investigator = h.get_role_user(Investigator)
    spy = h.get_role_user(Spy)
    computer_scientist = h.get_role_user(ComputerScientist)
    astronaut = next(u for u in game.users if not game.get_astronaut(u).roles)

    cmd_game_act(mutant, {
        "action": "VOTE_MUTATE",
        "targetId": doctor_1.id
    })

    cmd_game_act(mutant, {
        "action": "VOTE_PARALYSE",
        "targetId": doctor_2.id
    })

    cmd_game_act(psychologist, {
        "action": "PSYCHOLOGIST_ANALYSE",
        "targetId": traitor.id
    })

    cmd_game_act(computer_scientist, {
        "action": "COUNT_MUTANTS"
    })

    cmd_game_act(geneticist, {
        "action": "GENETICIST_ANALYSE",
        "targetId": mutant.id
    })

    cmd_game_act(hacker, {
        "action": "HACK",
        "hackedRole": "PSYCHOLOGIST"
    })

    cmd_game_act(indic, {
        "action": "INDIC_INVESTIGATE",
        "targetId": traitor.id
    })

    cmd_game_act(spy, {
        "action": "SPY",
        "targetId": traitor.id
    })

    if execute_someone:
        vote_victime = [traitor, computer_scientist, hacker, doctor_1, doctor_2, geneticist, indic]

        for voter in vote_victime:
            cmd(voter, {
                "action": "VOTE_EXECUTION",
                "targetId": astronaut.id
            })

    h.set_nb_event_checkpoint()
    cmd(doctor_2, {
        "action": "END_EXECUTION_VOTES"
    }, ignore_session_token=True)

    assert game.game_engine.phase.ACTIVE_ROLE == Mutant
    assert not game.get_astronaut(doctor_2).paralysed
    report = h.events_contain_event(h.get_events_after_checkpoint(), "VOTE_RESULT")[0].payload
    if execute_someone:
        assert report["target"]["id"] == astronaut.id
        assert not game.get_astronaut(astronaut).alive

    else:
        assert report["target"] is None
        assert game.get_astronaut(astronaut).alive
    # End of the first round
