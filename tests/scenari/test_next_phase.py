from src.engine.phase import FirstMutantPhase, SecondMutantPhase
from src.engine.roles.computer_scientist import ComputerScientist
from src.engine.roles.mutant import Mutant


def test_another_player_next_phase(scenario_helper):
    h = scenario_helper
    game = h.game
    cmd = h.cmd
    cmd_game_act = h.cmd_game_act

    mutant = h.get_role_user(Mutant)
    computer_scientist = h.get_role_user(ComputerScientist)
    astronaut = next(u for u in game.users if not game.get_astronaut(u).roles)

    assert game.game_engine.phase.ACTIVE_ROLE == Mutant
    assert not game.game_engine.phase.is_over
    cmd(mutant, {
        "action": "VOTE_MUTATE",
        "targetId": astronaut.id
    })

    assert type(game.game_engine.phase) is FirstMutantPhase
    assert game.game_engine.phase.is_over
    assert not game.game_engine.phase.all_next_phase_ready()

    # another player do NEXT_PHASE
    cmd(computer_scientist, {
        "action": "NEXT_PHASE"
    })

    assert type(game.game_engine.phase) is FirstMutantPhase
    assert game.game_engine.phase.is_over
    assert not game.game_engine.phase.all_next_phase_ready()

    # the good player does it
    cmd(mutant, {
        "action": "NEXT_PHASE"
    })

    assert type(game.game_engine.phase) is SecondMutantPhase
    assert not game.game_engine.phase.is_over
    assert not game.game_engine.phase.all_next_phase_ready()

    # End of the first round
