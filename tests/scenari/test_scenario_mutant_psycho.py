from src.engine.roles.computer_scientist import ComputerScientist
from src.engine.roles.doctor import Doctor
from src.engine.roles.geneticist import Geneticist
from src.engine.roles.hacker import Hacker
from src.engine.roles.indic import Indic
from src.engine.roles.investigator import Investigator
from src.engine.roles.mutant import Mutant
from src.engine.roles.psychologist import Psychologist
from src.engine.roles.spy import Spy
from src.engine.roles.traitor import Traitor


def test_mutate_heal_psycho(scenario_helper):
    h = scenario_helper
    game = h.game
    cmd = h.cmd
    cmd_game_act = h.cmd_game_act

    mutant = h.get_role_user(Mutant)
    indic = h.get_role_user(Indic)
    psychologist = h.get_role_user(Psychologist)
    traitor = h.get_role_user(Traitor)
    doctor_1, doctor_2 = (u for u in game.users if game.get_astronaut(u).has_role(Doctor))
    geneticist = h.get_role_user(Geneticist)
    hacker = h.get_role_user(Hacker)
    investigator = h.get_role_user(Investigator)
    spy = h.get_role_user(Spy)
    computer_scientist = h.get_role_user(ComputerScientist)
    astronaut = next(u for u in game.users if not game.get_astronaut(u).roles)

    assert game.game_engine.round_number == 1
    h.set_nb_event_checkpoint()
    cmd_game_act(mutant, {
        "action": "VOTE_MUTATE",
        "targetId": astronaut.id
    })

    assert game.game_engine.round_number == 1
    cmd_game_act(mutant, {
        "action": "VOTE_BRAINWASH",
        "targetId": traitor.id
    })

    cmd_game_act(doctor_1, {
        "action": "HEAL",
        "targetId": astronaut.id
    })

    cmd_game_act(doctor_2, {
        "action": "HEAL",
        "targetId": mutant.id
    })

    h.set_nb_event_checkpoint()
    cmd_game_act(psychologist, {
        "action": "PSYCHOLOGIST_ANALYSE",
        "targetId": mutant.id
    })

    report = h.events_contain_event(h.get_events_after_checkpoint(), "PSYCHOLOGIST_ANALYSIS",
                                    recipient=[psychologist], target=mutant)[0]

    assert report.payload["mutant"]