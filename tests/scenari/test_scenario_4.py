from src.engine.roles.computer_scientist import ComputerScientist
from src.engine.roles.doctor import Doctor
from src.engine.roles.geneticist import Geneticist
from src.engine.roles.hacker import Hacker
from src.engine.roles.indic import Indic
from src.engine.roles.investigator import Investigator
from src.engine.roles.mutant import Mutant
from src.engine.roles.psychologist import Psychologist
from src.engine.roles.spy import Spy
from src.engine.roles.traitor import Traitor
from tests.scenari.conftest import Helper, log_all, setup_game
from tests.server.test_login import FakeWebsocket
from tests.server.test_start_game import NAMES


def test_mutate_first_doctor(server):
    websocket = FakeWebsocket()
    h = Helper(server)
    cmd = h.cmd
    cmd_game_act = h.cmd_game_act

    log_all(h, NAMES[:12], websocket)
    setup_game(h, {
        "Geneticist": 2
    })

    game = h.game

    mutant = h.get_role_user(Mutant)
    indic = h.get_role_user(Indic)
    psychologist = h.get_role_user(Psychologist)
    traitor = h.get_role_user(Traitor)
    doctor_1, doctor_2 = (u for u in game.users if game.get_astronaut(u).has_role(Doctor))
    geneticist_1, geneticist_2 = (u for u in game.users if game.get_astronaut(u).has_role(Geneticist))
    hacker = h.get_role_user(Hacker)
    investigator = h.get_role_user(Investigator)
    spy = h.get_role_user(Spy)
    computer_scientist = h.get_role_user(ComputerScientist)
    # astronaut = next(u for u in game.users if not game.get_astronaut(u).roles)

    cmd_game_act(mutant, {
        "action": "VOTE_MUTATE",
        "targetId": doctor_1.id
    })

    cmd_game_act(mutant, {
        "action": "VOTE_BRAINWASH",
        "targetId": traitor.id
    })

    cmd_game_act(doctor_2, {
        "action": "HEAL",
        "targetId": doctor_1.id
    })

    cmd_game_act(psychologist, {
        "action": "PSYCHOLOGIST_ANALYSE",
        "targetId": traitor.id
    })

    cmd_game_act(computer_scientist, {
        "action": "COUNT_MUTANTS"
    })

    cmd_game_act(geneticist_1, {
        "action": "GENETICIST_ANALYSE",
        "targetId": mutant.id
    })

    cmd_game_act(geneticist_2, {
        "action": "GENETICIST_ANALYSE",
        "targetId": hacker.id
    })

    cmd_game_act(hacker, {
        "action": "HACK",
        "hackedRole": "PSYCHOLOGIST"
    })

    cmd_game_act(indic, {
        "action": "INDIC_INVESTIGATE",
        "targetId": traitor.id
    })

    cmd_game_act(spy, {
        "action": "SPY",
        "targetId": traitor.id
    })

    cmd(geneticist_1, {
        "action": "VOTE_EXECUTION",
        "targetId": mutant.id
    })

    cmd(geneticist_2, {
        "action": "VOTE_EXECUTION",
        "targetId": geneticist_1.id
    })
    cmd(mutant, {
        "action": "VOTE_EXECUTION",
        "targetId": indic.id
    })
    white_votes = [traitor, computer_scientist, hacker, doctor_1, doctor_2, indic, psychologist,
                   investigator, spy]
    for voter in white_votes:
        cmd(voter, {
            "action": "VOTE_EXECUTION",
            "targetId": None
        })

    assert game.game_engine.phase.ACTIVE_ROLE == Mutant
    # End of the first round
