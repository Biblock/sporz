from src.server.command import Commands
from tests.server import get_events


class FakeWebsocket:
    pass


def test_login(server):
    assert len(server.users) == 0
    isaac_ws = FakeWebsocket()
    azazel_ws = FakeWebsocket()
    Commands({
        "action": "LOGIN",
        "name": "Isaac"
    }, server, isaac_ws).execute()
    assert len(server.users) == 1
    isaac = server.users[0]
    assert isaac.logged
    assert isaac.name == "Isaac"
    assert isaac.session.websocket == isaac_ws
    assert len(server.events) == 2
    notify_events = get_events(server, "LOGIN", [])
    session_token_events = get_events(server, "LOGIN", [isaac])
    assert len(notify_events) == 1
    assert len(session_token_events) == 1

    assert session_token_events[0].payload["sessionToken"] == isaac.session.id

    server.events = []

    # Azazel log in with another websocket
    Commands({
        "action": "LOGIN",
        "name": "Azazel"
    }, server, azazel_ws).execute()
    assert len(server.users) == 2

    azazel = next(user for user in server.users if user.name == "Azazel")

    notify_events = get_events(server, "LOGIN", [isaac])
    assert len(notify_events) == 1
    assert "sessionToken" not in notify_events[0].payload
    session_token_events = get_events(server, "LOGIN", [azazel])
    assert len(session_token_events) == 1
    assert session_token_events[0].payload["sessionToken"] == azazel.session.id
    assert azazel.session.id != isaac.session.id
    assert azazel.session.websocket == azazel_ws
    assert azazel.session.websocket != isaac.session.websocket

    server.events = []

    # Lilith logs in with Azazel's websocket (same client)

    Commands({
        "action": "LOGIN",
        "name": "Lilith"
    }, server, azazel_ws).execute()

    lilith = next(user for user in server.users if user.name == "Lilith")

    notify_events = get_events(server, "LOGIN", [isaac, azazel])
    session_token_events = get_events(server, "LOGIN", [lilith])
    assert len(notify_events) == 1
    assert len(session_token_events) == 1
    assert lilith.session.id != azazel.session.id
    assert lilith.session.websocket == azazel.session.websocket

    # Isaac logs out but reuse its session_token and is logged again

    assert isaac.logged
    server.logout(isaac.session.websocket)
    assert not isaac.logged

    Commands({
        "action": "GET_LOBBY",
        "sessionToken": isaac.session.id
    }, server, azazel_ws).execute()
    assert isaac.logged
