import pytest

from src.engine.phase import FirstMutantPhase
from src.server.command import Commands
from src.server.exceptions import NotTheGameAdminError, NotEnoughPlayersError
from tests.server.test_login import FakeWebsocket

NAMES = ["Aurore", "Bryan", "Charlie", "Dylan", "Enzo", "Farid", "Gabrielle", "Hugo", "Ines", "Jade", "Kevin", "Lucie",
         "Marie", "Noemie", "Oscar", "Pavel", "Quentin", "Renaud", "Sophie", "Tony", "Ulysse", "Valentin", "William",
         "Xavier", "Yannis", "Zoe"]
OTHER_NAMES = [f"{name}_2" for name in NAMES]


@pytest.fixture
def server_game_started(server_with_game, game_isaac, game_rocket_league, isaac):
    common_ws = FakeWebsocket()
    for game, names in (game_isaac, NAMES), (game_rocket_league, OTHER_NAMES):
        for name in names:
            Commands({
                "action": "LOGIN",
                "name": name
            }, server_with_game, common_ws).execute()
        users = [user for user in server_with_game.users if user.name in names]
        for user in users:
            Commands({
                "action": "JOIN_GAME",
                "gameId": game.id,
                "sessionToken": user.session.id
            }, server_with_game, user.session.websocket).execute()

        Commands({
            "action": "START_GAME",
            "gameId": game.id,
            "sessionToken": isaac.session.id
        }, server_with_game, isaac.session.websocket).execute()

    return server_with_game


@pytest.fixture
def started_game(server_game_started):
    return server_game_started.games[0]


@pytest.fixture
def other_started_game(server_game_started):
    return server_game_started.games[1]


def test_start_game(server_with_game, game_isaac, isaac, azazel):
    Commands({
        "action": "JOIN_GAME",
        "gameId": game_isaac.id,
        "sessionToken": isaac.session.id
    }, server_with_game, isaac.session.websocket).execute()

    with pytest.raises(NotTheGameAdminError):
        Commands({
            "action": "START_GAME",
            "gameId": game_isaac.id,
            "sessionToken": azazel.session.id
        }, server_with_game, azazel.session.websocket).execute()

    with pytest.raises(NotEnoughPlayersError):
        Commands({
            "action": "START_GAME",
            "gameId": game_isaac.id,
            "sessionToken": isaac.session.id
        }, server_with_game, isaac.session.websocket).execute()

    # Just so the game has enough player to start
    for i in range(20):
        Commands({
            "action": "LOGIN",
            "name": f"Kuriboh{i}"
        }, server_with_game, azazel.session.websocket).execute()
    for kuriboh in (u for u in server_with_game.users if u.name.startswith("Kuriboh")):
        Commands({
            "action": "JOIN_GAME",
            "gameId": game_isaac.id,
            "sessionToken": kuriboh.session.id
        }, server_with_game, kuriboh.session.websocket).execute()

    assert not game_isaac.started
    for user in game_isaac.users:
        assert game_isaac.get_astronaut(user) is None
    Commands({
        "action": "START_GAME",
        "gameId": game_isaac.id,
        "sessionToken": isaac.session.id
    }, server_with_game, isaac.session.websocket).execute()

    assert game_isaac.started
    assert type(game_isaac.game_engine.phase) == FirstMutantPhase
    for user in game_isaac.users:
        assert game_isaac.get_astronaut(user) is not None
