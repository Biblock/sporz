from src.engine.astronaut import Genome
from src.engine.roles.mutant import Mutant
from src.server.command import Commands


def test_vote_mutate(server_game_started, started_game):
    mutant_user = next(user for user in started_game.users if started_game.get_astronaut(user).has_role(Mutant))
    target_user = next(user for user in started_game.users
                       if not started_game.get_astronaut(user).has_role(Mutant)
                       and started_game.get_astronaut(user).genome != Genome.RESISTANT)
    assert not started_game.get_astronaut(target_user).has_role(Mutant)
    Commands({
        "action": "VOTE_MUTATE",
        "gameId": started_game.id,
        "targetId": target_user.id,
        "sessionToken": mutant_user.session.id
    }, server_game_started, mutant_user.session.websocket).execute()
    assert started_game.get_astronaut(target_user).has_role(Mutant)
