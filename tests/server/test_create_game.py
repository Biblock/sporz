import pytest

from src.server.command import Commands
from src.server.exceptions import MissingArgumentError, UserNotLoggedError, SessionTokenWebsocketMismatchError
from tests.server import get_events


def test_create_game_failure(populated_server, isaac, azazel):
    with pytest.raises(MissingArgumentError):
        Commands({
            "action": "CREATE_GAME",
            "gameName": "The Binding of Isaac"
        }, populated_server, isaac.session.websocket).execute()

    with pytest.raises(MissingArgumentError):
        Commands({
            "action": "CREATE_GAME",
            "sessionToken": isaac.session.id
        }, populated_server, isaac.session.websocket).execute()

    with pytest.raises(UserNotLoggedError):
        Commands({
            "action": "CREATE_GAME",
            "sessionToken": "comment est votre blanquette ?",
            "gameName": "The Binding of Isaac"
        }, populated_server, isaac.session.websocket).execute()

    populated_server.logout(isaac.session.websocket)


def test_create_game(populated_server, isaac, azazel, lilith):
    assert len(populated_server.games) == 0
    Commands({
        "sessionToken": isaac.session.id,
        "action": "CREATE_GAME",
        "gameName": "The Binding of Isaac"
    }, populated_server, isaac.session.websocket).execute()

    assert len(populated_server.games) == 1
    game = populated_server.games[0]
    assert game.admin == isaac

    assert len(get_events(populated_server, "GAME_CREATED", [isaac, azazel, lilith])) == 1
