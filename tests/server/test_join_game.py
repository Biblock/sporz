import pytest

from src.server.command import Commands
from src.server.exceptions import NoSuchGameError


def test_join_game_failure(server_with_game, game_isaac, isaac):
    with pytest.raises(NoSuchGameError):
        Commands({
            "action": "JOIN_GAME",
            "gameId": "Oriflamme",
            "sessionToken": isaac.session.id
        }, server_with_game, isaac.session.websocket).execute()


def test_join_game(server_with_game, game_isaac, isaac):
    Commands({
        "action": "JOIN_GAME",
        "gameId": game_isaac.id,
        "sessionToken": isaac.session.id
    }, server_with_game, isaac.session.websocket).execute()

    assert set(game_isaac.users) == {isaac}
