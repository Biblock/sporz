import pytest

from src.engine.exceptions import NoRoomLeftError, NoSuchRoomError
from src.server.command import Commands


def enter_room(game, server, user, room_name):
    Commands({
        "action": "ROOM_ENTER",
        "gameId": game.id,
        "roomName": room_name,
        "sessionToken": user.session.id
    }, server, user.session.websocket).execute()


def check_permission(game, server, user, room_name):
    Commands({
        "action": "ROOM_PERMISSION",
        "gameId": game.id,
        "roomName": room_name,
        "sessionToken": user.session.id
    }, server, user.session.websocket).execute()


def check_enter_room(game, server, user, room_name, initial_room_name=None, expected_fullness=False):
    astronaut = game.get_astronaut(user)
    if initial_room_name is None:
        assert astronaut.room is None
    else:
        assert astronaut.room.name == initial_room_name
    check_permission(game, server, user, room_name)
    event = game.events[-1]
    assert event.payload["event"] == "ROOM_PERMISSION"
    assert event.payload["room_full"] is expected_fullness
    assert event.payload["room_name"] == room_name

    enter_room(game, server, user, room_name)
    assert astronaut.room.name == room_name
    event = game.events[-1]
    assert event.payload["event"] == "ENTER_ROOM"
    assert event.payload["room_name"] == room_name


def test_rooms(server_game_started, started_game):
    a1, a2, a3, a4, a5 = list(started_game.users)[:5]
    with pytest.raises(NoSuchRoomError) as e:
        check_enter_room(started_game, server_game_started, a1, "Non existing room")
    assert e.value.room_name == "Non existing room"

    check_enter_room(started_game, server_game_started, a1, "MACHINES")
    check_enter_room(started_game, server_game_started, a2, "MACHINES")
    check_enter_room(started_game, server_game_started, a3, "MACHINES")
    check_enter_room(started_game, server_game_started, a4, "MACHINES")
    with pytest.raises(NoRoomLeftError):
        check_enter_room(started_game, server_game_started, a5, "MACHINES", expected_fullness=True)

    check_enter_room(started_game, server_game_started, a1, "COCKPIT", initial_room_name="MACHINES")

    check_enter_room(started_game, server_game_started, a5, "MACHINES")
