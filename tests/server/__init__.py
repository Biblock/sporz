def get_events(server, event, recipients):
    return [e for e in server.events if set(e.recipients) == set(recipients) and e.payload["event"] == event]