import pytest

from src.server.command import Commands
from src.server.exceptions import NoSuchActionError
from src.server.server import Server
from tests.server.test_login import FakeWebsocket


@pytest.fixture
def server():
    return Server(None, None)


@pytest.fixture
def populated_server(server):
    Commands({
        "action": "LOGIN",
        "name": "Isaac"
    }, server, FakeWebsocket()).execute()
    Commands({
        "action": "LOGIN",
        "name": "Azazel"
    }, server, FakeWebsocket()).execute()
    Commands({
        "action": "LOGIN",
        "name": "Lilith"
    }, server, FakeWebsocket()).execute()
    return server


@pytest.fixture
def server_with_game(populated_server, isaac):
    Commands({
        "sessionToken": isaac.session.id,
        "action": "CREATE_GAME",
        "gameName": "The Binding of Isaac"
    }, populated_server, isaac.session.websocket).execute()
    Commands({
        "sessionToken": isaac.session.id,
        "action": "CREATE_GAME",
        "gameName": "Rocket League"
    }, populated_server, isaac.session.websocket).execute()
    return populated_server


@pytest.fixture
def game_isaac(server_with_game):
    return server_with_game.games[0]


@pytest.fixture
def game_rocket_league(server_with_game):
    return server_with_game.games[1]


@pytest.fixture
def isaac(populated_server):
    return next(u for u in populated_server.users if u.name == "Isaac")


@pytest.fixture
def azazel(populated_server):
    return next(u for u in populated_server.users if u.name == "Azazel")


@pytest.fixture
def lilith(populated_server):
    return next(u for u in populated_server.users if u.name == "Lilith")


def test_bad_action(server):
    with pytest.raises(NoSuchActionError):
        Commands({
            "action": "ALL_IN"
        }, server, None).execute()
