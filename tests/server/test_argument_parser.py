import pytest

from src.server.argument_parser import ArgumentParser
from src.server.exceptions import MissingArgumentError, UserNotLoggedError


class ServerMock:
    def get_user_from_session_token(self, token):
        return None


def test_argument_parser():
    expected_lyrics = "DONNE-MOI TON BON VIEUX FUNK, TON ROCK BABY, TA SOUL BABY"
    parser = ArgumentParser({
        "name": "K.Maro",
        "lyrics": expected_lyrics
    }, None)
    name, lyrics, rival = parser.get_arguments("name", "lyrics", "rival", nullable=["rival"])
    assert name == "K.Maro"
    assert lyrics == expected_lyrics
    assert rival is None

    with pytest.raises(MissingArgumentError):
        parser.get_arguments("failure")

    parser = ArgumentParser({
        "sessionToken": "K.PONE INC"
    }, ServerMock())
    with pytest.raises(UserNotLoggedError):
        parser.get_sender()
