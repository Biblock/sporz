from src.engine.engine_events import VoteMutate
from src.engine.gameprofile import GameProfile
from src.engine.roles.hacker import Hacker
from src.engine.roles.geneticist import Geneticist
from src.engine.roles.indic import Indic
from src.engine.roles.investigator import Investigator
from src.engine.roles.spy import Spy
from src.engine.roles.traitor import Traitor
from src.engine.roles.computer_scientist import ComputerScientist
from src.engine.roles.psychologist import Psychologist
from src.server.command import Commands


def test_convert(server_with_game, game_isaac, isaac, azazel, lilith):
    for u in isaac, azazel, lilith:
        Commands({
            "action": "JOIN_GAME",
            "gameId": game_isaac.id,
            "sessionToken": u.session.id
        }, server_with_game, u.session.websocket).execute()

    game_isaac.game_profile = GameProfile(
        [Psychologist, ComputerScientist, Geneticist, Hacker, Indic, Investigator, Spy, Traitor])
    Commands({
        "action": "START_GAME",
        "gameId": game_isaac.id,
        "sessionToken": isaac.session.id
    }, server_with_game, isaac.session.websocket).execute()
    game_engine = game_isaac.game_engine

    ast_isaac = game_isaac.get_astronaut(isaac)
    ast_azazel = game_isaac.get_astronaut(azazel)
    converted = game_isaac.convert_event(VoteMutate([game_engine.astronauts], ast_isaac, ast_azazel))

    assert set(*converted["recipients"]) == {isaac, azazel, lilith}
    assert isaac.to_json() == converted["voter"]
    assert azazel.to_json() == converted["target"]
