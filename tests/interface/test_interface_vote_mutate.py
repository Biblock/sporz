import pytest

from src.engine.exceptions import CanNotHaveRole
from src.engine.roles.mutant import Mutant


def test_vote_kill_other_mutant(game):
    interface = game.interface
    ast_mutant = game.get_all(Mutant, as_astronaut=True).pop()
    mutant = ast_mutant.as_role(Mutant)
    with pytest.raises(CanNotHaveRole) as e:
        interface.vote_mutate(ast_mutant, ast_mutant)
        ast_mutant.vote_mutate(ast_mutant.astronaut)
    assert e.value.role == Mutant
    assert mutant.kill_vote is None
