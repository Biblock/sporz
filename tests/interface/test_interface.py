import pytest

from src.engine.exceptions import AstronautsCanNotBeFromDifferentGamesError
from src.engine.roles.mutant import Mutant


def test_target_another_game_astronaut(game, another_game):
    mutant_game_1 = game.get_all(Mutant, as_astronaut=True).pop()
    astronaut_game_2 = next(ast for ast in another_game.astronauts if not ast.has_role(Mutant))
    interface = game.interface
    with pytest.raises(AstronautsCanNotBeFromDifferentGamesError):
        interface.vote_mutant_kill(mutant_game_1, astronaut_game_2)
