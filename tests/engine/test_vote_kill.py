from src.engine.astronaut import Genome
from src.engine.engine_events import MutantKill
from src.engine.roles.mutant import Mutant
from tests.engine.test_game import get_events


def test_vote_kill_ok(game, mutant):
    assert len(get_events(game, MutantKill)) == 0
    other_astronaut = next(a for a in game.astronauts if not a.has_role(Mutant))
    mutant.vote_kill(other_astronaut)
    assert not other_astronaut.alive
    assert get_events(game, MutantKill)[0].target == other_astronaut


def test_vote_kill_multiple_mutants(game, mutant):
    a2, a3, *_ = (a for a in game.astronauts if not a.has_role(Mutant) and a.genome != Genome.RESISTANT)
    mutant2 = Mutant(a2)
    a2.roles.add(mutant2)
    assert mutant.mutation_vote is mutant2.mutation_vote is None

    mutant.vote_kill(a3)
    assert mutant.kill_vote == a3
    assert mutant2.kill_vote is None
    assert a3.alive

    mutant2.vote_kill(a3)
    assert not a3.alive


def test_vote_kill_multiple_mutants_disagreeing_then_agreeing(game, mutant):
    a2, a3, a4, *_ = (a for a in game.astronauts if not a.has_role(Mutant) and a.genome != Genome.RESISTANT)
    mutant2 = Mutant(a2)
    a2.roles.add(mutant2)
    assert mutant.kill_vote is mutant2.kill_vote is None

    mutant.vote_kill(a3)
    assert mutant.kill_vote == a3
    assert mutant2.kill_vote is None
    assert a3.alive

    mutant2.vote_kill(a4)
    assert mutant.kill_vote == a3
    assert mutant2.kill_vote == a4
    assert a3.alive

    mutant2.vote_kill(a3)
    assert not a3.alive
