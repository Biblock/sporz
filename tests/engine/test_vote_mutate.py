from src.engine.astronaut import Genome
from src.engine.engine_events import ResistedMutation, Mutation
from src.engine.roles.mutant import Mutant


def get_events(game, t):
    return [e for e in game.observer.events if type(e) == t]


def test_vote_mutate_ok(game, mutant):
    assert len(get_events(game, Mutation)) == 0
    other_astronaut = next(a for a in game.astronauts if not a.has_role(Mutant) and a.genome != Genome.RESISTANT)
    mutant.vote_mutate(other_astronaut)
    assert other_astronaut.has_role(Mutant)
    assert get_events(game, Mutation)[0].target == other_astronaut


def test_vote_mutate_resistant(game, mutant):
    other_astronaut_resistant = next(p for p in game.astronauts
                                     if not p.has_role(Mutant) and p.genome == Genome.RESISTANT)

    assert len(get_events(game, ResistedMutation)) == 0
    mutant.vote_mutate(other_astronaut_resistant)
    assert not other_astronaut_resistant.has_role(Mutant)
    assert get_events(game, ResistedMutation)[0].target == other_astronaut_resistant


def test_vote_multiple_mutants(game, mutant):
    a2, a3, *_ = (a for a in game.astronauts if not a.has_role(Mutant) and a.genome != Genome.RESISTANT)
    mutant2 = Mutant(a2)
    a2.roles.add(mutant2)
    assert mutant.mutation_vote is mutant2.mutation_vote is None

    mutant.vote_mutate(a3)
    assert mutant.mutation_vote == a3
    assert mutant2.mutation_vote is None
    assert not a3.has_role(Mutant)

    mutant2.vote_mutate(a3)
    assert a3.has_role(Mutant)


def test_vote_multiple_mutants_disagreeing_then_agreeing(game, mutant):
    a2, a3, a4, *_ = (a for a in game.astronauts if not a.has_role(Mutant) and a.genome != Genome.RESISTANT)
    mutant2 = Mutant(a2)
    a2.roles.add(mutant2)
    assert mutant.mutation_vote is mutant2.mutation_vote is None

    mutant.vote_mutate(a3)
    assert mutant.mutation_vote == a3
    assert mutant2.mutation_vote is None
    assert not a3.has_role(Mutant)

    mutant2.vote_mutate(a4)
    assert mutant.mutation_vote == a3
    assert mutant2.mutation_vote == a4
    assert not a3.has_role(Mutant)

    mutant2.vote_mutate(a3)
    assert a3.has_role(Mutant)
