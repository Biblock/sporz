import pytest

from src.engine.astronaut import Genome
from src.engine.game_engine import GameEngine
from src.engine.gameprofile import GameProfile
from src.engine.roles.geneticist import Geneticist
from src.engine.roles.mutant import Mutant


class FakeObserver:
    def __init__(self):
        self.events = []

    def add_event(self, event):
        self.events.append(event)


def get_events(game, t):
    return [e for e in game.observer.events if type(e) == t]


@pytest.fixture
def game(started_game):
    game_engine = started_game.game_engine
    game_engine.observer = FakeObserver()
    return game_engine


@pytest.fixture
def another_game(other_started_game):
    game_engine = other_started_game.game_engine
    game_engine.observer = FakeObserver()
    return game_engine


@pytest.fixture
def non_mutant_resistant_astronaut(game):
    return next(a for a in game.astronauts if not a.has_role(Mutant) and a.genome != Genome.RESISTANT)


@pytest.fixture
def mutant(game):
    return game.get_all(Mutant).pop()


def test_game():
    game = GameEngine(FakeObserver())
    assert len(game.astronauts) == 13


def test_game_astronauts_numbers():
    game = GameEngine(FakeObserver(), GameProfile(initial_roles_number={
        Geneticist: 3
    }))
    assert len(game.astronauts) == 15
    assert len([g for g in game.astronauts if g.has_role(Geneticist)]) == 3
