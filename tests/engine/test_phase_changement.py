from src.engine.phase import FirstMutantPhase, SecondMutantPhase
from src.engine.roles.mutant import Mutant


def test_phase_change_vote_mutate(game, non_mutant_resistant_astronaut):
    mutant = game.get_all(Mutant, as_astronaut=True).pop()
    assert type(game.phase) is FirstMutantPhase
    assert not game.phase.is_over
    game.interface.vote_mutate(mutant, non_mutant_resistant_astronaut)
    game.interface.next_phase(mutant)
    assert type(game.phase) is SecondMutantPhase


def test_phase_change_vote_kill(game, non_mutant_resistant_astronaut):
    mutant = game.get_all(Mutant, as_astronaut=True).pop()
    assert type(game.phase) is FirstMutantPhase
    assert not game.phase.is_over
    game.interface.vote_mutant_kill(mutant, non_mutant_resistant_astronaut)
    game.interface.next_phase(mutant)
    assert type(game.phase) is SecondMutantPhase
