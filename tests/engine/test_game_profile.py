from src.engine.gameprofile import GameProfile
from src.engine.roles.geneticist import Geneticist
from src.engine.roles.hacker import Hacker
from src.engine.roles.psychologist import Psychologist
from src.engine.roles.doctor import Doctor
from src.engine.roles.mutant import Mutant


def test_minimum_player_number():
    assert GameProfile().min_initial_astronaut_number == 11

    gp = GameProfile(disabled_roles=[Mutant, Doctor])
    assert gp.min_initial_astronaut_number == 11

    gp = GameProfile(disabled_roles=[Mutant, Doctor, Psychologist])
    assert gp.min_initial_astronaut_number == 10

    gp = GameProfile(disabled_roles=[Psychologist])
    assert gp.min_initial_astronaut_number == 10

    gp = GameProfile(disabled_roles=[Psychologist, Hacker])
    assert gp.min_initial_astronaut_number == 9

    gp = GameProfile(initial_roles_number={Geneticist: 2})
    assert gp.min_initial_astronaut_number == 12

    gp = GameProfile(initial_roles_number={Geneticist: 4})
    assert gp.min_initial_astronaut_number == 14

    gp = GameProfile(initial_roles_number={Psychologist: 1})
    assert gp.min_initial_astronaut_number == 11

    gp = GameProfile(initial_roles_number={Psychologist: 1, Doctor: 2})
    assert gp.min_initial_astronaut_number == 11

    gp = GameProfile(initial_roles_number={Doctor: 3})
    assert gp.min_initial_astronaut_number == 12

    gp = GameProfile(initial_roles_number={Doctor: 2})
    assert gp.min_initial_astronaut_number == 11

    gp = GameProfile(initial_roles_number={Doctor: 1})
    assert gp.min_initial_astronaut_number == 10

    gp = GameProfile(initial_roles_number={Doctor: 0})
    assert gp.min_initial_astronaut_number == 10

    gp = GameProfile(initial_roles_number={Psychologist: 1, Doctor: 1})
    assert gp.min_initial_astronaut_number == 10

    Doctor.MANDATORY = False
    gp = GameProfile(initial_roles_number={Doctor: 0})
    assert gp.min_initial_astronaut_number == 9


def test_specific_role_count():
    pass
